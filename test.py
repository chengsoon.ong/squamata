"""Checking that things are working with spearman.py"""

import numpy as np
import scipy.stats as sps
from spearman import spearman_rho
from rank import extend_array
from rank import OrderedList

def create_data1(verbose=True):
    items = np.array(['a','b','c','d','e','f','g','h','i'])
    R = np.array([1,2,3,4,5,6,7,8,9])
    S = np.array([1,2,3,6,4,7,9,5,8])
    T = np.array([1,2,9,8,7,6,5,4,3])
    if verbose:
        print(items)
        print('R = %s' % str(R))
        print('S = %s' % str(S))
        print('T = %s' % str(T))
    return R, S, T, items

def pack(all_lists):
    R, S, T = all_lists
    rank_lists = np.zeros((len(R),3))
    rank_lists[:,0] = R
    rank_lists[:,1] = S
    rank_lists[:,2] = T
    return rank_lists

def create_data2():
    items = np.array(['a','b','c','d','e','f'])
    rank_lists = np.zeros((6,16))
    rank_lists[:,0]  = np.array([1,2,3,4,5,6])
    rank_lists[:,1]  = np.array([1,2,3,4,6,5])
    rank_lists[:,2]  = np.array([1,2,3,5,4,6])
    rank_lists[:,3]  = np.array([1,2,3,5,6,4])
    rank_lists[:,4]  = np.array([1,2,3,6,5,4])
    rank_lists[:,5]  = np.array([1,2,3,6,4,5])
    rank_lists[:,6]  = np.array([6,5,4,3,2,1])
    rank_lists[:,7]  = np.array([6,5,4,3,1,2])
    rank_lists[:,8]  = np.array([6,5,4,2,3,1])
    rank_lists[:,9]  = np.array([6,5,4,2,1,3])
    rank_lists[:,10] = np.array([6,5,4,1,2,3])
    rank_lists[:,11] = np.array([6,5,4,1,3,2])
    rank_lists[:,12] = np.array([4,1,2,3,5,6])
    rank_lists[:,13] = np.array([5,1,2,3,4,6])
    rank_lists[:,14] = np.array([5,1,2,3,6,4])
    rank_lists[:,15] = np.array([4,1,2,3,6,5])

    return rank_lists, items

def test1():
    print('# Check that for two lists, Spearman is the same as scipy.stats.pearsonr')
    R,S,T,items = create_data1()
    rank_lists = pack((R,S,T))
    print('Correlation between R,S,T = %f' % spearman_rho(rank_lists))
    print('rho(R,S), rho(R,T), rho(S,T)')
    print('Copula based estimate')
    print(spearman_rho(rank_lists[:,(0,1)]),
        spearman_rho(rank_lists[:,(0,2)]),
        spearman_rho(rank_lists[:,(1,2)]))
    print('Using scipy.pearsonr')
    print(sps.pearsonr(rank_lists[:,0],rank_lists[:,1])[0],
        sps.pearsonr(rank_lists[:,0],rank_lists[:,2])[0],
        sps.pearsonr(rank_lists[:,1],rank_lists[:,2])[0])


def test2():
    print('# Check that extension is working')
    R,S,T,items = create_data1()
    R_top = items[R<=4]
    S_top = items[S<=5]
    T_top = items[T<=4]
    union_items = np.union1d(np.union1d(R_top, S_top), T_top)
    R_extend = extend_array(R_top, R[R<=4], union_items)
    S_extend = extend_array(S_top, S[S<=5], union_items)
    T_extend = extend_array(T_top, T[T<=4], union_items)
    print(union_items)
    print(R_extend)
    print(S_extend)
    print(T_extend)
    rank_lists = pack((R_extend, S_extend, T_extend))
    print('Correlation between R[:4], S[:5], T[:4] = %f' % spearman_rho(rank_lists))
    

def test3():
    print('# Check that OrderedList is working')
    R,S,T,items = create_data1()
    R_top = items[R<=4]
    S_top = items[S<=5]
    T_top = items[T<=4]
    union_items = np.union1d(np.union1d(R_top, S_top), T_top)
    A = OrderedList(R_top, R[R<=4], big_is_top=False)
    B = OrderedList(S_top, S[S<=5], big_is_top=False)
    C = OrderedList(T_top, T[T<=4], big_is_top=False)
    A.extend(union_items)
    B.extend(union_items)
    C.extend(union_items)
    A.sort_by_name()
    B.sort_by_name()
    C.sort_by_name()
    print(A)
    print(B)
    print(C)
    rank_lists = pack((A.ranks, B.ranks, C.ranks))
    print('Correlation between R[:4], S[:5], T[:4] = %f' % spearman_rho(rank_lists))
    
if __name__ == '__main__':
    test1()
    test2()
    test3()
