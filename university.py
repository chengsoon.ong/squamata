"""University ranking experiment"""

import numpy as np
import pandas as pd
from matplotlib.pyplot import figure, savefig, show
from spearman import spearman_rho
from expttools import get_xticks
from rank import RankTable
from rank import geo_mean

def top_spearman_emp(rank_lists, col_names):
    """Compute the top-k Spearman correlation for all values of k
    k chooses the length of each list.
    """
    rho = []
    for k in range(rank_lists.num_sample):
        if k in [0,1]:
            rho.append(0)
            continue
        top_k_ranks = rank_lists.data[col_names].copy()
        for col in col_names:
            top_k_ranks.loc[top_k_ranks[col]>k, col] = np.nan
        top_k_ranks.dropna(axis=0, how='all', inplace=True)
        top_k_ranks['Name'] = top_k_ranks.index
        table = RankTable(top_k_ranks)
        table.impute_missing()
        r = spearman_rho(table.get_ranks(), normalise=False)
        rho.append(r)
        del table
        del top_k_ranks
    return np.array(rho)

def plot_spearman(results):
    """Plot Spearman's correlation at various values of k.
    results is a DataFrame
    """
    fig = figure()
    ax = fig.add_subplot(111)
    symbols = np.array(['ks','g*','bx','c+'])
    top_k = get_xticks(results.shape[0])
    for ix,name in enumerate(results.columns.values):
        ax.semilogx(top_k, results[name][top_k], symbols[ix][0], linewidth=2)
        ax.semilogx(top_k, results[name][top_k], symbols[ix], label=name)
        
    ax.grid()
    ax.set_xlabel('Top $k$ universities')
    ax.set_ylabel('$\\rho$')
    ax.set_title("Spearman's $\\rho$ for university ranking (all years)")
    ax.legend(loc='lower center', numpoints=1)
    ax.set_ylim([0.1,1])
    #show()
    savefig('university/uni_correlation_allyears.pdf')
    
def plot_spearman_year(results):
    """Plot Spearman's correlation at various values of k.
    results is a DataFrame
    """
    fig = figure()
    ax = fig.add_subplot(111)
    symbols = np.array(['ks','g*','bx','c+'])
    top_k = get_xticks(results.shape[0])
    for ix,name in enumerate(results.columns.values):
        ax.semilogx(top_k, results[name][top_k], symbols[ix][0], linewidth=2)
        ax.semilogx(top_k, results[name][top_k], symbols[ix], label=name)
        
    ax.grid()
    ax.set_xlabel('Top $k$ universities')
    ax.set_ylabel('$\\rho$')
    ax.set_title("Spearman's $\\rho$ for university ranking (per year)")
    ax.legend(loc='lower center', numpoints=1)
    ax.set_ylim([0.1,1])
    #show()
    savefig('university/uni_correlation_peryear.pdf')
    

def plot_spearman2014(results):
    """Plot Spearman's correlation at various values of k.
    results is a DataFrame
    """
    fig = figure()
    ax = fig.add_subplot(111)
    symbols = np.array(['g*','bx','c+'])
    top_k = get_xticks(results.shape[0])
    for ix,val in enumerate(['rhoQS','rhoQT','rhoST']):
        ax.semilogx(top_k, results[val][top_k], symbols[ix][0])
        ax.semilogx(top_k, results[val][top_k], symbols[ix], label=val)
    ax.semilogx(top_k, results['rhoOP'][top_k], 'r', linewidth=2)
    ax.semilogx(top_k, results['rhoOP'][top_k], 'ro', label='optimistic')
    ax.semilogx(top_k, results['rho'][top_k], 'b', linewidth=2)
    ax.semilogx(top_k, results['rho'][top_k], 'bs', label='empirical')
        
    ax.grid()
    ax.set_xlabel('Top $k$ universities')
    ax.set_ylabel('$\\rho$')
    ax.set_title("Spearman's $\\rho$ for university ranking")
    ax.legend(loc='lower center', numpoints=1)
    #show()
    savefig('university/uni_correlation.pdf')
    

def top_spearman2014(rank_lists):
    """Compute the top-k Spearman correlation for all values of k.
    Optimistic since it compares to the geometric mean

    Computes Spearman for all 3 rankings, and all pairwise too.
    """
    assert(rank_lists.shape[1] == 3)
    cols = ['rho','rhoQS','rhoQT','rhoST','worst rank','rhobar']
    results = pd.DataFrame(columns=cols, index=range(rank_lists.shape[0]))
    for k in results.index:
        if k in [0,1]:
            results.loc[k,'rhoOP'] = 0
            results.loc[k,'rhoQS'] = 0
            results.loc[k,'rhoQT'] = 0
            results.loc[k,'rhoST'] = 0
            results.loc[k,'worst rank'] = 1
            continue
        results.loc[k,'rhoOP'] = spearman_rho(rank_lists[:k,:])
        results.loc[k,'rhoQS'] = spearman_rho(rank_lists[:k,(0,1)])
        results.loc[k,'rhoQT'] = spearman_rho(rank_lists[:k,(0,2)])
        results.loc[k,'rhoST'] = spearman_rho(rank_lists[:k,(1,2)])
        results.loc[k,'worst rank'] = np.max(rank_lists[:k,:])
    for k in results.index:
        results.loc[k,'rhobar'] = np.max(results.loc[k:,'rhoOP'])
    return results

def university2014():
    """Compute the geometric mean of university rankings,
    and plot rho for different top-k subsets.
    """
    col_names = ['qs2013','shanghai2014','times2014']
    table = pd.read_table('university/all_ranks.txt')
    all_ranks = RankTable(table)
    all_ranks.impute_missing()

    gm = geo_mean(all_ranks.get_ranks(col_names))
    all_ranks.add_rank('geo_mean', gm)
    all_ranks.sort_by('geo_mean')
    print(all_ranks.data.head(100).to_string())

    rank_lists = all_ranks.get_ranks(col_names)
    rho = top_spearman2014(rank_lists)
    rho['rho'] = top_spearman_emp(all_ranks, col_names)
    print(rho)
    plot_spearman2014(rho)

    for name in rho.columns.values:
        all_ranks.data[name] = np.array(rho[name])
    handle = open('university/uni_ranks.csv','w')
    all_ranks.data.to_csv(handle, index=False)
    handle.close()

    
def university():
    """Compute the geometric mean of university rankings,
    and plot rho for different top-k subsets.
    """
    table = pd.read_table('university/allyears.txt')
    all_ranks = RankTable(table)
    all_ranks.impute_missing()

    gm = geo_mean(all_ranks.get_ranks())
    all_ranks.add_rank('geo_mean', gm)
    all_ranks.sort_by('geo_mean')

    rho = pd.DataFrame(columns=['rho','rhoQ','rhoS','rhoT'], index=range(all_ranks.num_sample))
    rho['rho'] = top_spearman_emp(all_ranks, all_ranks.experts)
    rho['rhoQ'] = top_spearman_emp(all_ranks, all_ranks.experts_contain('qs'))
    rho['rhoS'] = top_spearman_emp(all_ranks, all_ranks.experts_contain('shanghai'))
    rho['rhoT'] = top_spearman_emp(all_ranks, all_ranks.experts_contain('times'))
    print(rho.head(20))
    plot_spearman(rho)

    rho_year = pd.DataFrame(columns=['all','2011','2012','2013'], index=range(all_ranks.num_sample))
    rho_year['all'] = top_spearman_emp(all_ranks, all_ranks.experts)
    rho_year['2011'] = top_spearman_emp(all_ranks, all_ranks.experts_contain('2011'))
    rho_year['2012'] = top_spearman_emp(all_ranks, all_ranks.experts_contain('2012'))
    rho_year['2013'] = top_spearman_emp(all_ranks, all_ranks.experts_contain('2013'))
    print(rho_year.head(20))
    plot_spearman_year(rho_year)

    for name in rho.columns.values:
        all_ranks.data[name] = np.array(rho[name])
    for name in rho_year.columns.values:
        all_ranks.data[name] = np.array(rho_year[name])
    handle = open('university/uni_ranks_allyears.csv','w')
    all_ranks.data.to_csv(handle, index=False)
    handle.close()
    


    
if __name__ == '__main__':
    university()
