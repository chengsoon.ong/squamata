"""Converting from scores to ranks and back.

Scores are real numbers, defaulting to larger scores being better.
Ranks are also real numbers (allowing ties), with 1 being the highest rank.
Objects can be any python object.
"""

import numpy as np
import pandas as pd

class OrderedList(object):
    """Container for a list of ordered objects"""
    def __init__(self, names, scores, big_is_top=True):
        assert(len(names) == len(scores))
        self.names = np.array(names)
        self.scores = np.array(scores)
        self.big_is_top = big_is_top

        if big_is_top:
            sort_idx = np.argsort(-scores)
        else:
            sort_idx = np.argsort(scores)
        self.ranks = np.argsort(sort_idx) + 1
    
    @property
    def num_items(self):
        return len(self.names)

    @property
    def n_ranks(self):
        return self.ranks/(self.num_items+1.)

    def __repr__(self):
        return 'names: %s\nranks: %s' % (str(self.names), str(self.ranks))
    
    def extend(self, all_items):
        """Extends current object to include the elements in array all_items.
        The ranks in the current object are maintained.
        It is assumed that the current object contains the top ranks,
        and all_items that are not in the current object are given
        ranks that are the average value of the remaning bottom ranks.
        """
        num_total = len(all_items)
        imputed_rank = 0.5*(self.num_items + num_total + 1)
        new_items = np.setdiff1d(all_items, self.names)
        old_num_items = self.num_items
        self.names = np.concatenate([self.names, new_items])
        old_scores = self.scores.copy()
        self.scores = np.nan*np.ones(self.num_items)
        self.scores[:old_num_items] = old_scores
        old_ranks = self.ranks.copy()
        self.ranks = np.empty(self.num_items)
        self.ranks[:old_num_items] = old_ranks
        self.ranks[old_num_items:] = imputed_rank

    def sort_by_name(self):
        """Sort by the names"""
        sort_idx = np.argsort(self.names)
        self.names = self.names[sort_idx]
        self.scores = self.scores[sort_idx]
        self.ranks = self.ranks[sort_idx]
        
class RankTable(object):
    """Container to store multiple rankings of the same objects"""
    def __init__(self, df):
        """df is a pandas dataframe.
        Assume that it contains a field called 'Name', which is the name of the objects.
        All other fields are assumed to be ranks
        """
        self.data = df

    @property
    def num_sample(self):
        return self.data.shape[0]

    @property
    def num_expert(self):
        return self.data.shape[1] - 1

    @property
    def experts(self):
        col_names = self.data.columns.values.tolist()
        col_names.remove('Name')
        return col_names

    def experts_contain(self, substring):
        return [s for s in self.experts if substring in s]
            
    def impute_missing(self):
        """Impute the values of NaN by filling them with
        the average of the bottom ranks"""
        for ex in self.experts:
            self._impute_rank(self.data[ex])

    def _impute_rank(self, ranks):
        """Returns the average value of bottom ranks"""
        num_ranked = np.max(ranks)
        imputed_rank = 0.5*(num_ranked + self.num_sample + 1)
        ranks[np.isnan(ranks)] = imputed_rank
        
    def get_ranks(self, experts=None, contains=None):
        """Return an array containing just the ranks.
        experts is a list of names of the columns.
        contains is a string. Columns which contain this string will be returned.
        """
        if experts is None and contains is None:
            return np.array(self.data.loc[:,self.experts].fillna(value = self.num_sample))
        elif type(experts) == type([]):
            return np.array(self.data.loc[:,experts].fillna(value = self.num_sample))
        elif type(contains) == type(''):
            match = self.experts_contain(contains)
            return np.array(self.data.loc[:,match].fillna(value = self.num_sample))
                
    def add_rank(self, name, ranks):
        """Add a new column with name name"""
        self.data[name] = pd.Series(ranks, index=self.data.index)

    def sort_by(self, name):
        """Sort rows in increasing order of column name"""
        self.data.sort(columns=name, inplace=True)
        
def geo_mean(data):
    """Return the geometric mean of the columns of data.
    The array data is assumed to be of size: num_sample by num_expert
    """
    num_sample, num_expert = map(float, data.shape)
    fdata = data.astype(float)/num_sample
    avg = np.zeros(int(num_sample))
    for cur_ex in range(int(num_expert)):
        avg += np.log(fdata[:,cur_ex])
    avg = num_sample * np.exp(avg/num_expert)
    sort_idx = np.argsort(avg)
    avg_rank = np.argsort(sort_idx) + 1
    return avg_rank


    
def extend_array(orig_items, orig_rank, union_items):
    """Returns a new array which is the ranks of items, that is an extension of orig_items.
    The ranks in orig_items are maintained.
    All missing ranks are given rank which is the average value of the remaining bottom ranks.
    """
    num_orig = len(orig_items)
    num_items = len(union_items)
    imputed_rank = 0.5*(num_orig+num_items+1)
    extended = np.zeros(num_items)
    for idx,elem in enumerate(union_items):
        found = np.flatnonzero(elem == orig_items)
        if len(found) == 0:
            extended[idx] = imputed_rank
        else:
            extended[idx] = orig_rank[found]
    return extended

def _rank_normalise(X, big_is_top):
    """X: array of shape (n_features, n_samples),
    big_is_top: boolean 
                controls whether a large value is ranked 1
                
    Rank normalisation is per column.
    
    returns the rank normalised array.
    """
    R = np.zeros(X.shape)
    for ex in range(X.shape[1]):
        if big_is_top:
            scores = -X[:,ex]
        else:
            scores = X[:,ex]
        sort_idx = np.argsort(scores)
        rev_sort = np.argsort(sort_idx)
        sorted_scores = scores[sort_idx]
        rank = []
        tied = False
        for idx,elem in enumerate(sorted_scores):
            if tied:
                rank.append(avg_rank)
                if idx >= end_ties:
                    tied = False
                continue
            ties = np.flatnonzero(elem == sorted_scores)
            if len(ties) == 1:
                rank.append(idx+1.)
            else:
                avg_rank = 0.5*(2*idx+len(ties)+1)
                rank.append(avg_rank)
                tied = True
                end_ties = idx+len(ties)-1
        rank = np.array(rank)
        R[:,ex] = rank[rev_sort]
    return R

def rank_normalise(X, big_is_top=False, row_first=True, max_iter=100):
    """Iterate rank normalisation by alternating rows and column normalisation.
    big_is_top: boolean {default = False},
                controls whether a large value is ranked 1
    row_first: boolean {default = True},
               start with row normalisation
    max_iter: integer {default = 100}
              number of iterations of row and column normalisation
            
    returns the rank normalised array.
    """
    R = X.copy()
    for ix in range(max_iter):
        old_R = R.copy()
        if row_first:
            R = _rank_normalise(R.T, big_is_top).T
        R = _rank_normalise(R, big_is_top)
        R = _rank_normalise(R.T, big_is_top).T
        if np.allclose(R,old_R):
            break
    R = _rank_normalise(R, big_is_top)
    #print('Number of normalisation iterations = %d' % (ix+1))
    return R
