Squamata
========

A python playground for ranking and ordering, using copula modeling


* university : University ranking from QS, Shanghai and Times
* gene-interaction : Combining HiC data with literature mining
* gwis : Stability of cross validation for bivariate GWAS
* spearmeans: Generalisation of k-means clustering to multivariate
  copulas
  
