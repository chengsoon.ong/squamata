"""Check intuitions on multivariate Spearman correlation"""

from rank import geo_mean
from spearman import spearman_rho

def compute_spearman(X, item_idx):
    rho = spearman_rho(X[:,item_idx])
    print('Spearman of %s = %f' % (str(item_idx), rho))
    print('Geometric mean')
    print(geo_mean(X[:,item_idx]))

if __name__ == '__main__':
    import test
    rank_lists, items = test.create_data2()
    compute_spearman(rank_lists, [0,6,12])
    compute_spearman(rank_lists, [0,1])
    compute_spearman(rank_lists, [0,1,2])
    compute_spearman(rank_lists, [2,3])
