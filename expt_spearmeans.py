"""Comparing k-means methods.

Performance criteria (smaller is better):
- 1-Normalized Rand
- van Dongen
- Variation of Information

Also report inertia relative to the one of the classification labels.

4 datasets:
glass, iris, vehicle, wine

3 methods:
- spearmeans
- k-means using shogun (with normalisation or not)

Normalisation for k-means:
- original values
- linear scaling to [0,1]

Initialisation:
- kmeans++
- mean of class (arithmetic and geometric respectively)

Experimental design:
- Repeat 100 times
- box plots

Based on experimental protocol in:
M. Emre Celebi, Hassan A. Kingravi, Patricio A. Vela
A Comparative Study of Efficient Initialization Methods for the K-Means Clustering Algorithm
Expert Systems with Application, 40(1): 200-210, 2013

Performance measures from:
Junjie Wu, Hui Xiong, Jian Chen
Adapting the Right Measures for K-means Clustering
KDD'09

"""
import time
import itertools
import scipy
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from optwok.mldata import Dataset
from optwok.expttools import apply_to_combination as atc
from spearmeans import SpearMeans, NoMeans
from rank import rank_normalise, geo_mean

def expt_clustering(data_dir, work_dir, datasets, perm_idx, methods, stats):
    """Doing the actual work of running all the clustering methods"""
    param = {'data_name': datasets,
             'perm': perm_idx,
             'method': methods}
    fparam = {'data_dir': data_dir}

    result = atc(run_clustering, param, fparam, multiproc=True)

    print('Convert the list of results into a Pandas Dataframe')
    row_idx = pd.MultiIndex.from_product([datasets, perm_idx], names=['dataset','permutation'])
    col_idx = pd.MultiIndex.from_product([methods, stats], names=['method','stats'])
    perf = pd.DataFrame(columns=col_idx, index=row_idx)
    for res in result:
        insert_result(res['data_name'], res['perm'], res['output'], res['method'], perf)
    print('Saving to %s/clusters.csv' % work_dir)
    perf.to_csv('%s/clusters.csv' % work_dir)


def run_clustering(data_dir='.', data_name='dummy', method='nomeans', perm=-1, init='random'):
    """Load data and run clustering benchmark.

    perm is ignored.
    """
    print('Data: %s, split=%d, method=%s' % (data_name, perm, method))
    data = Dataset(data_name, data_file='%s/%s_raw.csv' % (data_dir, data_name),
                   perm_file='%s/%s_perm.txt' % (data_dir, data_name))
    results = {'data_name': data_name, 'perm': perm, 'method': method}

    if method in ['nomeans','spearmeans','spear arith','kmeans rank']:
        # Convert data to ranks
        R = rank_normalise(data.examples, max_iter=1)
    
    #cheat_centroids = cheat_init(data)
    #clus_id = np.array(data.labels.copy(), dtype=int)-1
    if method == 'nomeans':
        results['output'] = bench_nomeans(R, data.num_class, init)
    elif method == 'spearmeans':
        results['output'] = bench_spearmeans(R, data.num_class, init)
    elif method == 'spear arith':
        results['output'] = bench_spearmeans(R, data.num_class, init, centroid='arithmean')
    elif method == 'kmeans raw':
        results['output'] = bench_skl(data.examples, data.num_class, init)
    elif method == 'kmeans unit':
        results['output'] = bench_skl(normalise(data.examples), data.num_class, init)
    elif method == 'kmeans rank':
        results['output'] = bench_skl(R, data.num_class, init)
    else:
        raise ValueError('Unknown method')

    return results

def bench_nomeans(X, n_clusters, init):
    """Run the NoMeans clustering algorithm

    Assume data X has been converted into ranks.
    
    returns the predictions on X, inertia and time taken.

    """
    start = time.clock()
    model = NoMeans(n_clusters=n_clusters, init=init)
    model.fit(X)
    pred = model.predict(X)
    delta = time.clock() - start
    return (pred, model.inertia, model.iterations, delta)    

def bench_spearmeans(X, n_clusters, init, centroid='geomean'):
    """Run the Spearmeans clustering algorithm,
    on data X with n_clusters, initialised with 'init'

    Assume data X has been converted into ranks.
    
    returns the predictions on X, inertia and time taken.
    """
    start = time.clock()
    model = SpearMeans(n_clusters=n_clusters, centroid=centroid, init=init)
    model.fit(X)
    pred = model.predict(X)
    delta = time.clock() - start
    return (pred, model.inertia, model.iterations, delta)

def inertia(X, centers):
    """
    helper to compute inertia with euclidean distance
    """
    n_samples = X.shape[0]
    k = centers.shape[0]
    from sklearn import metrics
    distances = metrics.euclidean_distances(centers, X, None,
                                            squared=True)
    z = np.empty(n_samples, dtype=np.int)
    z.fill(-1)
    mindist = np.empty(n_samples)
    mindist.fill(np.infty)
    for q in range(k):
        dist = np.sum((X - centers[q]) ** 2, axis=1)
        z[dist < mindist] = q
        mindist = np.minimum(dist, mindist)
    inertia = mindist.sum()
    return inertia

    
def bench_shogun(X, n_clusters, init, normalise=False):
    """Perform k-means using shogun"""
    from shogun.Distance import EuclidianDistance
    from shogun.Features import RealFeatures
    from shogun.Clustering import KMeans
    start = datetime.now()
    feat = RealFeatures(X)
    distance = EuclidianDistance(feat, feat)
    clf = KMeans(n_clusters, distance)
    clf.train()
    delta = datetime.now() - start
    return inertia(X, clf.get_cluster_centers()), Delta

def bench_skl(X, n_clusters, init, normalise=False):
    """Perform k-means using scikits.learn"""
    from sklearn import cluster as skl_cluster    
    start = time.clock()
    clf = skl_cluster.KMeans(n_clusters=n_clusters, n_init=1, init=init)
    clf.fit(X.T)
    delta = time.clock() - start
    return (clf.labels_, inertia(X.T, clf.cluster_centers_), clf.n_iter_, delta)

def normalise(X):
    """Normalise X by a linear transformation to the range [0,1]"""
    n_features, n_samples = X.shape
    N = np.zeros(X.shape)
    for feat in range(n_features):
        smallest = np.min(X[feat,:])
        biggest = np.max(X[feat,:])
        N[feat,:] = (X[feat,:]-smallest)/(biggest-smallest)
    return N

def cheat_init(data):
    """Use the labels to determine clusters, then return the centroids"""
    centroids = np.zeros((data.num_features, data.num_class))
    R = rank_normalise(data.examples, max_iter=1)
    for clus_id in range(data.num_class):
        idx = np.flatnonzero(data.labels == (clus_id+1))
        centroids[:,clus_id] = geo_mean(R[:,idx])
    return centroids

def calc_cont_mat(P, C):
    """For two arrays of labels P and C,
    compute the contingency matrix containing the counts
    of the number of objects in cluster P from class C.
    """
    assert len(P) == len(C), 'number or elements in P and C must be the same'
    n_cluster = len(np.unique(P))
    n_class = len(np.unique(C))
    #assert np.allclose(np.unique(P), range(n_cluster)), 'missing clusters in P'
    #assert np.allclose(np.unique(C), range(n_class)), 'missing clusters in C'
    cont_mat = np.zeros((n_cluster, n_class))
    for ip in range(n_cluster):
        for ic in range(n_class):
            cont_mat[ip,ic] = np.sum(np.logical_and(P == ip, C == ic))
    return cont_mat

def rand_statistic(cont_mat):
    """For the contingency matrix cont_mat,
    return the normalised Rand statistic

    The value is in [0,1], smaller is better
    """
    row_sum = np.sum(cont_mat, axis=0)
    col_comb = np.sum(map(scipy.misc.comb, row_sum, itertools.repeat(2, len(row_sum))))
    col_sum = np.sum(cont_mat, axis=1)
    row_comb = np.sum(map(scipy.misc.comb, col_sum, itertools.repeat(2, len(col_sum))))
    all_comb = np.sum(map(scipy.misc.comb, cont_mat.flatten(),
                          itertools.repeat(2, len(cont_mat.flatten()))))
    n = np.sum(cont_mat)
    ratio = col_comb*row_comb/scipy.misc.comb(n,2)
    rand = 1 - (all_comb - ratio)/(0.5*(col_comb + row_comb) - ratio)
    return rand


def van_dongen_crit(cont_mat):
    """
    For the contingency matrix cont_mat,
    return the van Dongen criterion
    """
    n = np.sum(cont_mat)
    sm1 = np.sum(np.max(cont_mat, axis=0))
    sm2 = np.sum(np.max(cont_mat, axis=1))
    ms1 = np.max(np.sum(cont_mat, axis=0))
    ms2 = np.max(np.sum(cont_mat, axis=1))
    vd = (2*n - sm1 - sm2)/(2*n - ms1 - ms2)
    return vd

def safe_x_log_x(x):
    """Assume that x is positive, returns x log(x) = 0 when x = 0."""
    l = np.zeros_like(x)
    l[x>0] = np.log(x[x>0])
    return x * l

def vari_information(cont_mat):
    """
    For the contingency matrix cont_mat,
    return the value of the variation of information.
    """
    assert np.min(cont_mat.shape) > 1, 'Only one cluster!'
    n = np.sum(cont_mat)
    prob_mat = cont_mat/n
    pi = np.sum(cont_mat, axis=1)/n
    pj = np.sum(cont_mat, axis=0)/n
    numerator = 0.
    for i in range(cont_mat.shape[0]):
        for j in range(cont_mat.shape[1]):
            try:
                if prob_mat[i,j] > 0:
                    numerator += prob_mat[i,j] * np.log(prob_mat[i,j]/(pi[i]*pj[j]))
            except IndexError:
                print prob_mat
                print i,j
    vi = 1 + 2*(numerator/(np.sum(safe_x_log_x(pi)) + np.sum(safe_x_log_x(pj))))
    return vi

def cluster_validity(data_dir, data_name, P, perm, verbose):
    """
    Load the labels from data_dir/data_name, this gives C.
    For two arrays of labels P and C,
    return a tuple with three measures of performance:
    - Rand statistic
    - van Dongen criterion
    - variation of information
    """
    data = Dataset(data_name, data_file='%s/%s_raw.csv' % (data_dir, data_name),
                   perm_file='%s/%s_perm.txt' % (data_dir, data_name))
    C = data.labels - 1.
    cont_mat = calc_cont_mat(P,C)
    if verbose:
        print(cont_mat)
    return (rand_statistic(cont_mat), van_dongen_crit(cont_mat), vari_information(cont_mat))

def insert_result(data_name, perm, result, method, perf, verbose=False):
    """Helper function to populate the results dataframe"""
    idx = pd.IndexSlice
    if verbose:
        print('%s iterations=%d, time=%f' % (method, result[2], result[3]))
    perf.loc[idx[data_name, perm], idx[method,'iterations']] = result[2]
    perf.loc[idx[data_name, perm], idx[method,'time taken']] = result[3]
    perf.loc[idx[data_name, perm], idx[method,'inertia']] = result[1]
    (rand, vd, vi) = cluster_validity(data_dir, data_name, result[0], perm, verbose)
    if verbose:
        print('RAND=%1.5f\tVD=%1.5f\tVI=%1.5f\n' % (rand, vd, vi))
    perf.loc[idx[data_name, perm], idx[method,'rand']] = rand
    perf.loc[idx[data_name, perm], idx[method,'vd']] = vd
    perf.loc[idx[data_name, perm], idx[method,'vi']] = vi

    
def _plot_stat(stat, perf, work_dir, datasets, datasize, methods):
    """Plot a subfigure for each dataset"""
    idx = pd.IndexSlice
    num_subfig = len(datasets)
    fig = plt.figure(figsize=(3*num_subfig+1,4))
    for ix,data_name in enumerate(datasets):
        ax = fig.add_subplot(1,num_subfig,ix+1)
        data = perf.loc[idx[data_name,:],:]
        plt.boxplot(data.values)
        #ax.set_title('%s: %s' % (data_name, datasize[data_name]))
        ax.set_title(data_name)
        ax.set_xticklabels(methods, rotation=60)
        if ix == 0:
            ax.set_ylabel(stat)
    plt.subplots_adjust(left=0.1, right=0.95, top=0.9, bottom=0.25)
    plt.savefig('%s/%s.pdf' % (work_dir, stat), bbox_inches='tight')
    
def plot_results(work_dir, datasets, datasize, perm_idx, methods, stats):
    print('Plotting results in %s' % work_dir)
    row_idx = pd.MultiIndex.from_product([datasets, perm_idx],
                                         names=['dataset','permutation'])
    col_idx = pd.MultiIndex.from_product([methods, stats],
                                         names=['method','stats'])
    perf = pd.DataFrame(columns=col_idx, index=row_idx)
    perf = perf.from_csv('%s/clusters.csv' % work_dir, header=[0,1], index_col=[0,1])
    idx = pd.IndexSlice
    for stat in stats:
        _plot_stat(stat, perf.loc[:,idx[:,stat]], work_dir, datasets, datasize, methods)
    #plt.show()
    
if __name__ == '__main__':
    data_dir = '/Users/cong/Data/multiclass/'
    work_dir = '/Users/cong/Repo/squamata/spearmeans/'
    datasize = {'glass': '9 features, 214 samples, 6 clusters',
                'iris': '4 features, 150 samples, 3 clusters',
                'urban_land_cover': '147 features, 675 samples, 9 clusters',
                'vehicle': '18 features, 94 samples, 4 clusters',
                'wine': '13 features, 178 samples, 3 clusters',
                }
                
    perm_idx = range(100)
    # Due to Pandas Dataframes, items have to be lexicographically sorted
    datasets = ['glass','iris','urban_land_cover','vehicle','wine']
    #methods = ['kmeans rank', 'kmeans raw', 'kmeans unit', 'nomeans', 'spear arith', 'spearmeans']
    methods = ['kmeans rank', 'kmeans raw', 'kmeans unit', 'nomeans', 'spearmeans']
    stats = ['inertia','iterations','rand','time taken','vd','vi']
    expt_clustering(data_dir, work_dir, datasets, perm_idx, methods, stats)

    # Things to present
    stats = ['iterations','rand','vd','vi']
    datasets = ['iris','wine','glass','vehicle','urban_land_cover']
    plot_results(work_dir, datasets, datasize, perm_idx, methods, stats)
