"""Spearman's correlation coefficient"""

import numpy as np

def h(d):
    """Normalization factor for d-dimensional Spearman's rho"""
    return (d+1.)/(2**d - (d+1.))

def spearman_rho(rank_lists, normalise=True):
    """Compute Spearman's rho for ranked lists.

    The input is assumed to be d lists of normalised ranks for n objects,
    with n rows and d columns.

    """
    n,d = rank_lists.shape
    assert d > 0, 'No ranked lists found'
    if d == 1:
        return 1.

    if normalise:
        n_rank = renormalise(rank_lists)
    else:
        n_rank = rank_lists.copy()
    n_rank /= n+1.
    prod = np.prod(1.-n_rank, axis=1)
    total = np.sum(prod)
    s_rho = h(d) * ((2**d/float(n)) * total - 1.)
    s_rho = s_rho*(n+1.)/(n-1.)
    return s_rho

def renormalise(orig_rank):
    """Compute ranks again"""
    n,d = orig_rank.shape
    new_rank = np.zeros((n,d))
    for idx in range(d):
        sort_idx = np.argsort(orig_rank[:,idx])
        new_rank[:,idx] = np.argsort(sort_idx) + 1.
    return new_rank

