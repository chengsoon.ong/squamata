"""Some small functions to help with experiments"""

def get_xticks(n):
    """Return a log spaced set of integers up to n"""
    max_val = np.ceil(np.log10(n))
    xticks = np.logspace(1,max_val, 40)
    xticks = np.round(xticks)
    too_many = xticks > n
    xticks = xticks[-too_many]
    xticks = np.concatenate((xticks, np.array([n])))
    return xticks

