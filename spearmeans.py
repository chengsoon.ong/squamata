"""Implement k-means clustering using Spearman correlation.
Interface follows that of sklearn.cluster.KMeans
"""
import numpy as np
import scipy.stats as sps
from rank import geo_mean
from spearman import spearman_rho

class SpearMeansBase(object):
    """k-means clustering using Spearman correlation"""
    def __init__(self, n_clusters=8, init='random', max_iter=300):
        """
        n_clusters : number of clusters to form
        init : {'k-means++', 'random', or an ndarray}
               Method for initialisation
        max_iter : Maximum number of iterations of the spearmeans algorithm
                   in a single run
        tol : relative tolerance to declare convergence
        """
        self.n_clusters = n_clusters
        self.init = init
        assert max_iter > 0, 'Maximum iterations need to be positive'
        self.max_iter = max_iter

    @property
    def labels(self):
        """
        The cluster which each point belongs to,
        available after fit is called
        """
        return np.nonzero(self._assignment.T)[1]

    def _inertia(self, X):
        """Return the objective function:
        the sum of the correlation of each cluster
        """
        inertia = 0.
        for cluster_idx in range(self.n_clusters):
            cur_X = X[:,self._assignment[cluster_idx,:]]
            inertia += spearman_rho(cur_X, normalise=False)
        return inertia
            
    def _tolerance(self, X, tol):
        """Return a tolerance which is independent of the dataset"""
        variances = np.var(X, axis=0)
        return np.mean(variances) * tol

    def kmeans_plusplus(self, X):
        """Initialise n_clusters seeds according to kmeans++

        Arthur, D. and Vassilvitskii, S.
        "k-means++: the advantages of careful seeding".
        ACM-SIAM symposium on Discrete algorithms. 2007
        """
        n_features, n_samples = X.shape
        centers = np.zeros((n_features, self.n_clusters))
        n_local_trials = 2 + int(np.log(self.n_clusters))
        center_id = np.random.randint(n_samples)
        centers[:,0] = X[:,center_id]
        all_correlations = spearman_corrs(centers[:,0], X)
        current_pot = all_correlations.sum()

        for center_id in range(1, self.n_clusters):
            rand_vals = np.random.random_sample(n_local_trials) * current_pot
            candidate_ids = np.searchsorted(all_correlations.cumsum(), rand_vals)
            candidate_ids[candidate_ids==n_samples] = n_samples-1
            candidate_corrs = spearman_corrs(X[:,candidate_ids], X)

            best_cand = None
            best_pot = None
            best_corr = None
            for trial in range(n_local_trials):
                new_corr = np.maximum(all_correlations, candidate_corrs[trial])
                new_pot = new_corr.sum()
                if (best_cand is None) or (new_pot > best_pot):
                    best_cand = candidate_ids[trial]
                    best_pot = new_pot
                    best_corr = new_corr
            centers[:,center_id] = X[:,best_cand]
            current_pot = best_pot
            all_correlations = best_corr
        return centers

class SpearMeans(SpearMeansBase):
    """k-means clustering using Spearman correlation"""
    def __init__(self, n_clusters=8, centroid='geomean', init='random', max_iter=300):
        """
        n_clusters : number of clusters to form
        centroid: {'geomean', 'arithmean'}
                  Geometric mean or arithmetic mean for computing centroid.
        init : {'k-means++', 'random', or an ndarray}
               Method for initialisation
        max_iter : Maximum number of iterations of the spearmeans algorithm
                   in a single run
        tol : relative tolerance to declare convergence
        """
        if hasattr(init, '__array__'):
            assert n_clusters == init.shape[1],\
                'n_clusters=%d but init has %d centers' % (n_clusters, init.shape[0])
            init = np.asarray(init, dtype=np.float64)

        SpearMeansBase.__init__(self, n_clusters, init, max_iter)
        self.centroid_method = centroid

    def fit(self, X, verbose=False):
        """Compute spearmeans clustering,
        X: ndarray of size n_features by n_samples
           The data.
        """
        n_features, n_samples = X.shape
        #self.tol = self._tolerance(X, self.tol)
        self._assignment = np.zeros((self.n_clusters, n_samples), dtype=bool)
        
        centers = self._init_centroids(X, self.init)
        self.centers = centers
        self.inertia =  None

        for ix in range(self.max_iter):
            centers_old = centers.copy()
            self._assign2center(X, centers)
            self.inertia = self._inertia(X)
            centers = self._estimate_mean(X)
            self.centers = centers.copy()
            if verbose:
                print(self.labels)
                print(self.inertia)
            if np.allclose(centers_old, centers):
                break
        self.iterations = ix+1

    def predict(self, X):
        """Predict the closest cluster each sample in X belongs to
        X: ndarray of size n_features by n_samples
           The data.

        returns labels: array of length n_samples
           The index of the cluster that each sample belongs to
        """
        self._assign2center(X, self.centers)
        return self.labels

    def _assign2center(self, X, centers):
        """Assign each point to the closest cluster.
        """
        n_samples = X.shape[1]
        all_correlations = spearman_corrs(centers, X)
        self._assignment.fill(False)
        for item_idx in range(n_samples):
            corr = all_correlations[:,item_idx]
            assert len(corr) == self.n_clusters
            max_idx = np.argmax(corr)
            self._assignment[max_idx, item_idx] = True

        if not np.all(np.sum(self._assignment, axis=1) > 0):
            empty_clus = np.flatnonzero(np.sum(self._assignment, axis=1) == 0)
            rand_points = np.random.permutation(n_samples)[:len(empty_clus)]
            for empty_id,idx in enumerate(rand_points):
                self._assignment[:,idx] = False
                self._assignment[empty_clus[empty_id],idx] = True

        assert np.all(np.sum(self._assignment, axis=0) == 1), 'Item assigned to more than one cluster'
        assert np.all(np.sum(self._assignment, axis=1) > 0), 'Empty cluster'

    def _estimate_mean(self, X):
        """Re-estimate the means of each cluster"""
        n_features = X.shape[0]
        centers = np.zeros((n_features, self.n_clusters))
        for center_id in range(self.n_clusters):
            cur_cluster = X[:, self._assignment[center_id,:]]
            if self.centroid_method == 'geomean':
                centers[:, center_id] = geo_mean(cur_cluster)
            elif self.centroid_method == 'arithmean':
                raw = np.mean(cur_cluster, axis=1)
                centers[:, center_id] = np.argsort(raw) + 1
            else:
                raise ValueError('Unknown centroid method %s' % self.centroid_method)
        return centers

    def _init_centroids(self, X, init):
        """Compute the initial centroids

        X: array, shape (n_features, n_samples)
        init: only 'random', 'k-means++' and array of centers is implemented

        returns an array of centers with shape(k, n_features)
        """
        n_samples = X.shape[1]
        if n_samples < self.n_clusters:
            raise ValueError("n_samples=%d should be larger than n_clusters=%d"
                            % (n_samples, self.n_clusters))
        if init == 'random':
            seeds = np.random.permutation(n_samples)[:self.n_clusters]
            centers = X[:,seeds]
        elif init == 'k-means++':
            centers = self.kmeans_plusplus(X)
        elif hasattr(init, '__array__'):
            centers = init
        else:
            raise ValueError("the init parameter for spearmeans should be "
                             "'random', 'k-means++', or an ndarray, "
                             "'%s' (type '%s') was passed." % (init, type(init)))

        return centers

class NoMeans(SpearMeansBase):
    """k-means clustering using Spearman correlation"""
    def __init__(self, n_clusters=8, init='random', max_iter=300):
        """
        n_clusters : number of clusters to form
        init : {'k-means++', 'random', or an ndarray}
               Method for initialisation
        max_iter : Maximum number of iterations of the spearmeans algorithm
                   in a single run
        tol : relative tolerance to declare convergence
        """
        SpearMeansBase.__init__(self, n_clusters, init='random', max_iter=max_iter)
        self.init = init 
        
    def fit(self, X, verbose=False):
        """Compute spearmeans clustering, without using centroids.
        X: ndarray of size n_features by n_samples
           The data.
        """
        n_features, n_samples = X.shape

        # Initialise assignments if given, otherwise initialise randomly
        self._assignment = np.zeros((self.n_clusters, n_samples), dtype=bool)
        if hasattr(self.init, '__array__'):
            clus_idx = self.init
        else:
            clus_idx = np.random.randint(0, self.n_clusters, size=n_samples)
        for idx in range(n_samples):
            self._assignment[clus_idx[idx],idx] = True

        if verbose:
            print(self.labels)
            print(self._inertia(X))

            
        # Optimise
        for ix in range(self.max_iter):
            labels_old = self.labels.copy()
            self._assign(X)
            if verbose:
                print(self.labels)
                print(self._inertia(X))
            if np.allclose(self.labels, labels_old):
                break
        self.inertia = self._inertia(X)
        self.iterations = ix+1
            
    def predict(self, X):
        """Predict the closest cluster each sample in X belongs to
        X: ndarray of size n_features by n_samples
           The data.

        returns labels: array of length n_samples
           The index of the cluster that each sample belongs to
        """
        self._assign(X)
        return self.labels

    def _assign(self, X):
        """Assign each point by computing multivariate Spearman
        to the cluster of examples"""
        n_features, n_samples = X.shape
        all_corr = np.zeros(self.n_clusters)
        for item_idx in np.random.permutation(n_samples):
            self._assignment[:,item_idx] = False
            for clus_idx in range(self.n_clusters):
                cur_clus = X[:,self._assignment[clus_idx,:]]
                cur_item = X[:,item_idx]
                cur_item.shape = (n_features,1)
                all_corr[clus_idx] = spearman_rho(np.hstack([cur_item,cur_clus]), normalise=False)
            closest_clus = int(np.argmax(all_corr))
            self._assignment[closest_clus, item_idx] = True
    

def spearman_corrs(A, B):
    """Compute all pairwise spearman correlations"""
    if len(A.shape) == 1:
        A.shape = (len(A),1)
    if len(B.shape) == 1:
        B.shape = (len(B),1)
    fA, nA = A.shape
    fB, nB = B.shape
    if fA != fB:
        raise ValueError("The number of features are different in A and B")
    corr, pval = sps.spearmanr(A, B, axis=0)
    return corr[:nA,nA:]

if __name__ == '__main__':
    import test
    rank_lists, items = test.create_data2()
    #model = SpearMeans(n_clusters=3, init=rank_lists[:,(0,6,12)])
    #model = SpearMeans(n_clusters=3, init='random')
    #model = NoMeans(n_clusters=3, init=np.array([0,0,0,0,0,0,1,1,1,1,1,1,2,2,2,2]))
    model = NoMeans(n_clusters=3, init='random')
    model.fit(rank_lists, verbose=True)
    print('Prediction:')
    #print(model.centers)
    print(model.predict(rank_lists))
    print('Inertia = %f' % model.inertia)
