"""Compute Spearman correlation and geometric mean aggregation for gwis stability experiments"""

import numpy as np
import pandas as pd
import bz2
import copy
import matplotlib.pyplot as plt
from gwis.pairs2id import get_rankfile_name
from spearman import spearman_rho
from expttools import get_xticks
from rank import RankTable
from rank import geo_mean

def load(file_name):
    data = pd.read_table(bz2.BZ2File(file_name, 'r'), sep=',')
    new_cols = data.columns.values
    new_cols[0] = 'Name'
    data.columns = new_cols
    data.drop(['prb1','prb2'], inplace=True, axis=1)
    return data

def top_spearman(rank_lists, top_k):
    """Compute the top-k Spearman correlation for given values of top_k.
    k chooses the value of the geometric mean.
    """
    rho = []
    for k in top_k:
        if k in [0,1]:
            rho.append(0)
            continue
        r = spearman_rho(rank_lists[:k,:])
        rho.append(r)
    return np.array(rho)

def top_spearman_emp(rank_lists, col_names, top_k):
    """Compute the top-k Spearman correlation for given values of top_k.
    k chooses the length of each list.
    """
    rho = []
    for k in top_k:
        if k in [0,1]:
            rho.append(0)
            continue
        top_k_ranks = rank_lists.data[col_names].copy()
        for col in col_names:
            top_k_ranks.loc[top_k_ranks[col]>k, col] = np.nan
        top_k_ranks.dropna(axis=0, how='all', inplace=True)
        top_k_ranks['Name'] = top_k_ranks.index
        table = RankTable(top_k_ranks)
        table.impute_missing()
        r = spearman_rho(table.get_ranks(), normalise=False)
        rho.append(r)
    return np.array(rho)

def top_spearman_biv(rank_lists, top_k, split):
    """Compute bivariate top-k Spearman on each pair of splits.
    k chooses the length of each list for given values of top_k.
    """
    rho = np.zeros((len(top_k), len(split)))
    for ixk,k in enumerate(top_k):
        if k in [0,1]:
            rho[ixk,:] = 0
            continue
        for s in split:
            col_names = []
            for c in cv:
                col_names.append('s%dc%d' % (s,c))
            top_k_ranks = rank_lists.data[col_names].copy()
            for col in col_names:
                top_k_ranks.loc[top_k_ranks[col]>k, col] = np.nan
            top_k_ranks.dropna(axis=0, how='all', inplace=True)
            top_k_ranks['Name'] = top_k_ranks.index
            table = RankTable(top_k_ranks)
            table.impute_missing()
            r = spearman_rho(table.get_ranks(), normalise=False)
            rho[ixk,s] = r
    return rho
        
def plot_spearman(top_k, rho_op, rho_emp, rho_biv, d, p, s):
    """Plot Spearman's correlation at various values of k.
    results is a DataFrame
    """
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.semilogx(top_k, rho_op, 'r')
    ax.semilogx(top_k, rho_op, 'ro', label='optimistic')
    ax.semilogx(top_k, rho_emp, 'b')
    ax.semilogx(top_k, rho_emp, 'bs', label='empirical')
    ax.semilogx(top_k, rho_biv, 'g')
    ax.semilogx(top_k, rho_biv, 'gx', label='bivariate')
        
    ax.grid()
    ax.set_xlabel('Top $k$ GWIS pairs')
    ax.set_ylabel("Spearman's $\\rho$")
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles[:3], labels[:3], numpoints=1)
    ax.set_ylim([-0.9,1.])
    ax.set_xlim([10,100000])

    title = '%sWTC' % d
    if p:
        title += 'p'
    title += '-%s' % s
    ax.set_title('Cross validation stability for %s' % title)
    #plt.show()
    plt.savefig('gwis/%s.pdf' % title)
    plt.close(fig)


def process(data):
    all_ranks = RankTable(data)
    all_ranks.impute_missing()

    gm = geo_mean(all_ranks.get_ranks())
    all_ranks.add_rank('geo_mean', gm)
    all_ranks.sort_by('geo_mean')
    #print(all_ranks.data.head(20).to_string())

    return all_ranks

if __name__ == '__main__':
    dataset = ['bd', 'cad', 'cd', 'ht', 'ra', 't1d', 't2d']
    prune = [False, True]
    split = range(10)
    cv = [1,2]
    stat = ['Chi2', 'GSS']

    data_dir = '/Users/cong/Data/WTCCC/top1000/'

    col_names = []
    for s in split:
        for c in cv:
            col_names.append('s%dc%d' % (s,c))

    
    for d in dataset:
        for p in prune:
            for s in stat:
                file_name = get_rankfile_name(d,p,s)
                print(file_name)
                data = load(data_dir+file_name)
                all_ranks = process(data)
                top_k = get_xticks(all_ranks.num_sample)
                rank_lists = all_ranks.get_ranks(col_names)
                rho_op = top_spearman(np.array(rank_lists), top_k)
                rho_emp = top_spearman_emp(all_ranks, col_names, top_k)
                rho_biv = top_spearman_biv(all_ranks, top_k, split)
                plot_spearman(top_k, rho_op, rho_emp, rho_biv, d, p, s)

