"""Hierarchical clustering.

Reference:
Modern hierarchical, agglomerative clustering algorithms
Daniel Muellner
arXiv:1109.2378v1
"""

import numpy as np
import scipy.stats as sps
from rank import geo_mean

def hclust(examples):
    """Returns a stepwise dendrogram corresponding to
    the hierarchical clustering of dataset examples.
    """
    dendro = []
    num_ex = examples.shape[1]
    loc_ex = examples.copy()
    names = list(range(num_ex))
    print(names)
    num_items = np.zeros(2*num_ex - 1, dtype=int)
    num_items[:num_ex] = 1
    for ix in range(num_ex-1):
        (a,b,s) = min_pair(names, loc_ex, num_items)
        num_items[num_ex+ix] = num_items[a] + num_items[b]
        dendro.append((a, b, 1.-s, num_items[num_ex+ix]))
        names, loc_ex = append(names, loc_ex, a, b, num_ex+ix)
        names, loc_ex = remove(names, loc_ex, a, b)
    return np.array(dendro)

def min_pair(idx, loc_ex, num_items):
    """Returns the pair of items with the maximum correlation,
    as well as the correlation.
    """
    num_ex = len(idx)
    a = 0
    b = 0
    s = -1.
    for ixa in range(num_ex):
        for ixb in range(ixa+1, num_ex):
            rho = sps.pearsonr(loc_ex[:,ixa], loc_ex[:,ixb])[0]
            if rho > s:
                a, b, s = idx[ixa], idx[ixb], rho
    return a, b, s

def remove(names, loc_ex, a, b):
    """Remove a and b from idx and loc_ex"""
    ixa = np.flatnonzero(np.array(names) == a)[0]
    ixb = np.flatnonzero(np.array(names) == b)[0]
    names.remove(a)
    names.remove(b)
    loc_ex = np.delete(loc_ex, np.array([ixa,ixb]), axis=1)
    return names, loc_ex

def append(names, loc_ex, a, b, n):
    """Append n to idx and loc_ex.
    Example n is created as the geometric mean of a and b.

    TODO: weight by number of items
    """
    names.append(n)
    ixa = np.flatnonzero(np.array(names) == a)[0]
    ixb = np.flatnonzero(np.array(names) == b)[0]
    new_ex = geo_mean(loc_ex[:,[ixa, ixb]])
    new_ex.shape = (len(new_ex), 1)
    loc_ex = np.append(loc_ex, new_ex, axis=1)
    return names, loc_ex
    
if __name__ == '__main__':
    import test
    import scipy
    import scipy.cluster.hierarchy
    from scipy.cluster.hierarchy import dendrogram
    import matplotlib.pyplot as plt
    rank_lists, items = test.create_data2()
    dendro = hclust(rank_lists)
    print(dendro)
    dendrogram(dendro)
    plt.show()
