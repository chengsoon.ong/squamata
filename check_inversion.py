import numpy as np
import scipy.stats as sps
from numpy.random import rand



def check_1d(n=1000, d=300):
    violations = 0
    for ix in range(n):
        a = rand(d,1)
        b = rand(d,1)
        c = rand(d,1)
        dab = np.dot((a-b).T, a-b)[0,0]
        dac = np.dot((a-c).T, a-c)[0,0]
        dbc = np.dot((b-c).T, b-c)[0,0]
        to_merge = np.argmin([dab, dac, dbc])
        print(dab, dac, dbc)
        if to_merge == 0:
            gm = sps.gmean(np.hstack([a,b]), axis=1)
            gm.shape = (d,1)
            dm = np.dot((gm-c).T, gm-c)
            lhs = np.min([dac, dbc])
            rhs = dm[0,0]
            if lhs > rhs:
                print(lhs, rhs)
                violations += 1
        elif to_merge == 1:
            gm = sps.gmean(np.hstack([a,c]), axis=1)
            gm.shape = (d,1)
            dm = np.dot((gm-b).T, gm-b)
            lhs = np.min([dab, dbc])
            rhs = dm[0,0]
            if lhs > rhs:
                print(lhs, rhs)
                violations += 1
        elif to_merge == 2:
            gm = sps.gmean(np.hstack([b,c]), axis=1)
            gm.shape = (d,1)
            dm = np.dot((gm-a).T, gm-a)
            lhs = np.min([dac, dab])
            rhs = dm[0,0]
            if lhs > rhs:
                print(lhs, rhs)
                violations += 1
    print('%d violations out of %d' % (violations, n))


if __name__ == '__main__':
    check_1d()
