
from __future__ import division
import gzip
import numpy as np

def load_data(data_file):
    print('Reading %s' % data_file)
    handle = gzip.open(data_file,'r')
    names = ['PubMedID','SNP','Disease','SNPchr','SNPloc','Gene','GeneWindow',
             'eQTL','HiCLR','Dist','GeneID','LitScore','dbSNPFreq']
    formats = [int,'S12','S30','S5',int,'S12','S30',float,float,int,int,float,int]
    data = np.genfromtxt(handle, delimiter='\t', skiprows=1,
                      dtype={'names': names, 'formats': formats})
    handle.close()

    print('Preprocess')
    all_sd = []
    for ix in range(len(data)):
        all_sd.append(data['SNP'][ix]+'|'+data['Disease'][ix])
    all_sd = np.unique(np.array(all_sd))
    return data, all_sd

def geo_mean(data1, data2, num_ex, unit=True):
    if len(data1) == 0:
        return np.array([])
    assert(np.max(data1) <= num_ex)
    assert(np.max(data2) <= num_ex)
    num_ex = float(num_ex)
    d1 = np.array(map(float, data1))/num_ex
    d2 = np.array(map(float, data2))/num_ex
    avg = num_ex*np.exp((np.log(d1)+np.log(d2))/2.)
    sort_idx = np.argsort(avg)
    avg_rank = np.argsort(sort_idx) + 1
    if unit:
        avg_rank = avg_rank/float(num_ex)
    return avg_rank

def geo_mean3(data1, data2, data3, num_ex, unit=True):
    if len(data1) == 0:
        return np.array([])
    assert(np.max(data1) <= num_ex)
    assert(np.max(data2) <= num_ex)
    assert(np.max(data3) <= num_ex)
    num_ex = float(num_ex)
    d1 = np.array(map(float, data1))/num_ex
    d2 = np.array(map(float, data2))/num_ex
    d3 = np.array(map(float, data3))/num_ex
    avg = num_ex*np.exp((np.log(d1)+np.log(d2)+np.log(d3))/3.)
    sort_idx = np.argsort(avg)
    avg_rank = np.argsort(sort_idx) + 1
    if unit:
        avg_rank = avg_rank/float(num_ex)
    return avg_rank

def get_rank_hic(raw_score, unit=True):
    sort_idx = np.argsort(-raw_score)
    hic_rank = np.argsort(sort_idx) + 1
    if unit:
        num_ex = len(raw_score)
        hic_rank = hic_rank/float(num_ex)
    return hic_rank

def get_rank_lit(raw_score, unit=True):
    score = -raw_score.copy()
    score[np.isnan(score)] = 10000.0
    sort_idx = np.argsort(score)
    lit_rank = np.argsort(sort_idx) + 1
    if unit:
        num_ex = len(raw_score)
        lit_rank = lit_rank/float(num_ex)
    return lit_rank

def get_rank_dist(raw_score, unit=True):
    score = raw_score.copy()
    neg_idx = score < 0
    score[neg_idx] = 500000000
    sort_idx = np.argsort(score)
    dist_rank = np.argsort(sort_idx) + 1
    dist_rank[neg_idx] = len(score) - len(np.flatnonzero(neg_idx)) + 1
    if unit:
        num_ex = len(raw_score)
        dist_rank = dist_rank/float(num_ex)
    return dist_rank

def get_rank_count(raw_score, unit=True):
    print('Nonzeros: %d' % len(np.flatnonzero(raw_score>0)))
    score = -raw_score.copy()
    sort_idx = np.argsort(score)
    count_rank = np.argsort(sort_idx) + 1
    no_count = score == 0
    count_rank[no_count] = len(score) - len(np.flatnonzero(no_count)) + 1
    if unit:
        num_ex = len(raw_score)
        count_rank = count_rank/float(num_ex)
    return count_rank
