"""Converting a table of SNP gene association into an interaction graph
in JSON format for Rede visualisation"""

import json
import numpy as np
import ranktools

def load_data(data_file):
    print('Reading %s' % data_file)
    handle = gzip.open(data_file,'r')
    names = ['PubMedID','SNP','Disease','SNPchr','SNPloc','Gene','GeneWindow',
             'eQTL','HiCLR','Dist','GeneID','LitScore','dbSNPFreq',
             'rankDist','rankHiCLR','rankLit','rankAvg','TotalEx']
    formats = [int,'S12','S30','S5',int,'S12','S30',
               float,float,int,int,float,int,
               int,int,int,int,int]
    data = np.genfromtxt(handle, delimiter='\t', skiprows=1,
                      dtype={'names': names, 'formats': formats})
    handle.close()

    print('Preprocess')
    all_sd = []
    for ix in range(len(data)):
        all_sd.append(data['SNP'][ix]+'|'+data['Disease'][ix])
    all_sd = np.unique(np.array(all_sd))
    return data, all_sd

def create_nodes(data):
    snp_names = np.unique(data['SNP'])
    gene_names = np.unique(data['Gene'])
    print('%d unique SNPs and %d unique Genes' % (len(snp_names), len(gene_names)))
    adj_mat = np.zeros((len(snp_names), len(gene_names)))
    for row in data:
        ixs = np.flatnonzero(row['SNP'] == snp_names)[0]
        ixg = np.flatnonzero(row['Gene'] == gene_names)[0]
        adj_mat[ixs,ixg] += 1
        
    nodes = []
    for node_idx,snp_name in enumerate(snp_names):
        idx = np.flatnonzero(data['SNP'] == snp_name)[0]
        row = data[idx]
        ixs = np.flatnonzero(row['SNP'] == snp_names)[0]
        ixg = np.flatnonzero(row['Gene'] == gene_names)[0]
        entry = {'prbCode': snp_name,
                 'degree': np.sum(adj_mat[ixs,:]),
                 'prb': node_idx,
                 'rs': snp_name,
                 'probe_group': 1,
                 'bp_position': row['SNPloc'],
                 'chrom': int(row['SNPchr'][3:]),
                 'id': node_idx
                 }
        nodes.append(entry)
    for node_idx,gene_name in enumerate(gene_names):
        idx = np.flatnonzero(data['Gene'] == gene_name)[0]
        row = data[idx]
        ixs = np.flatnonzero(row['SNP'] == snp_names)[0]
        ixg = np.flatnonzero(row['Gene'] == gene_names)[0]
        chr_str, loc_str = row['GeneWindow'].split(':')
        chrom = chrom2int(chr_str)
        start, finish = loc_str.split('-')
        loc = int(round(0.5*(int(start)+int(finish))))
        entry = {'prbCode': row['GeneID'],
                 'degree': np.sum(adj_mat[:,ixg]),
                 'prb': node_idx+len(snp_names),
                 'rs': gene_name,
                 'probe_group': 2,
                 'bp_position': loc,
                 'chrom': chrom,
                 'id': node_idx+len(snp_names)
                 }
        nodes.append(entry)
        
    return nodes

def create_links(data, all_sd):
    snp_names = np.unique(data['SNP'])
    gene_names = np.unique(data['Gene'])
    links = []
    for link_idx,row in enumerate(data):
        ixs = np.flatnonzero(row['SNP'] == snp_names)[0]
        ixg = np.flatnonzero(row['Gene'] == gene_names)[0]
        litscore = row['LitScore']
        if np.isnan(litscore):
            litscore = -100.
        eqtl = float(row['eQTL'])
        if eqtl > 0.0:
            eqtl = np.log10(eqtl)
        sd = row['SNP']+'|'+row['Disease']
        idx_sd = np.flatnonzero(all_sd == sd)[0] + 1
        entry = {'source': ixs,
                 'target': ixg+len(snp_names),
                 'probe_group': idx_sd,
                 'eQTL': eqtl,
                 'HiC': row['HiCLR'],
                 'Dist': float(row['Dist']),
                 'Lit': litscore,
                 'dbSNPFreq': float(row['dbSNPFreq'])
                 }
        links.append(entry)
    return links

def chrom2int(chr_str):
    chr_num = chr_str[3:]
    if chr_num == 'X':
        return 23
    elif chr_num == 'Y':
        return 24
    else:
        return int(chr_num)
    

def build_graph(data, all_sd):
    nodes = create_nodes(data)
    links = create_links(data, all_sd)
    print('Graph with %d nodes and %d links' % (len(nodes), len(links)))
    return nodes, links

def convert_tsv2json(tsv_name, json_name):
    data, all_sd = ranktools.load_data(tsv_name)
    print('%d lines in %s' % (len(data), tsv_name))
    #sd = all_sd[0]
    #snp, disease = sd.split('|')
    #idx = np.logical_and(data['SNP']==snp, data['Disease']==disease)
    nodes, links = build_graph(data, all_sd)
    graph = {'name': tsv_name, 'nodes': nodes, 'links': links}
    print('Writing to %s' % json_name)
    json.dump(graph, open(json_name,'w'))
    

if __name__ == '__main__':
    convert_tsv2json('top10_diffchrom_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz',
                     'top10_diffchrom.json')
