import csv
import gzip
import numpy as np
from optwok.io_pickle import load, save
import matplotlib.pyplot as plt
from ranktools import *


def eqtl_rank(orig_file, out_name):
    data, all_sd = load_data(orig_file)
    print('Generating top hits in %s' % out_name)
    out_file = csv.writer(gzip.open(out_name,'w'), delimiter='&')
    names = ['PubMedID','SNP','Disease','SNPchr','SNPloc','Gene','GeneWindow',
             'eQTL','HiCLR','Dist','GeneID','LitScore', 'dbSNPFreq',
             'rankDist','rankHiCLR','rankLit','rankAvg','Total']
    out_file.writerow(names)

    for sd in all_sd:
        snp, disease = sd.split('|')
        idx = np.logical_and(data['SNP']==snp, data['Disease']==disease)
        cur_data = data[idx]
        idx_eqtl = np.flatnonzero(cur_data['eQTL']>0)
        if len(idx_eqtl) == 0:
            continue
        elif len(idx_eqtl) > 1:
            print('Error!')

        num_ex = len(cur_data)
        dist_rank = get_rank_dist(cur_data['Dist'])
        hic_rank = get_rank_hic(cur_data['HiCLR'])
        lit_rank = get_rank_lit(cur_data['LitScore'])
        avg_rank = geo_mean3(dist_rank, hic_rank, lit_rank, num_ex)[idx_eqtl]
        dist_rank = dist_rank[idx_eqtl]
        hic_rank = hic_rank[idx_eqtl]
        lit_rank = lit_rank[idx_eqtl]

        # Assume only one SNP disease pair
        idx_line = 0
        line = list(cur_data[idx_eqtl[idx_line]])
        line.append(dist_rank[idx_line])
        line.append(hic_rank[idx_line])
        line.append(lit_rank[idx_line])
        line.append(avg_rank[idx_line])
        line.append(num_ex)
        out_file.writerow(line)

def top_avg(orig_file, out_name, k=100):
    data, all_sd = load_data(orig_file)
    print('Generating top hits in %s' % out_name)
    out_file = csv.writer(gzip.open(out_name,'w'), delimiter='\t')
    names = ['PubMedID','SNP','Disease','SNPchr','SNPloc','Gene','GeneWindow',
             'eQTL','HiCLR','Dist','GeneID','LitScore', 'dbSNPFreq',
             'rankDist','rankHiCLR','rankLit','rankAvg','Total']
    out_file.writerow(names)

    for sd in all_sd:
        snp, disease = sd.split('|')
        idx = np.logical_and(data['SNP']==snp, data['Disease']==disease)
        cur_data = data[idx]
        num_ex = len(cur_data)
        dist_rank = get_rank_dist(cur_data['Dist'])
        hic_rank = get_rank_hic(cur_data['HiCLR'])
        lit_rank = get_rank_lit(cur_data['LitScore'])
        #avg_rank = geo_mean(hic_rank, lit_rank, num_ex)
        avg_rank = geo_mean3(dist_rank, hic_rank, lit_rank, num_ex)
        idx_rank = np.argsort(avg_rank)[:k]

        for idx_line in idx_rank:
            line = list(cur_data[idx_line])
            line.append(dist_rank[idx_line])
            line.append(hic_rank[idx_line])
            line.append(lit_rank[idx_line])
            line.append(avg_rank[idx_line])
            line.append(num_ex)
            out_file.writerow(line)

def genevar_validation(orig_file, out_name):
    #22344756 & rs3213182  & Carcinoma, Squamous Cell & TP53 
    table_rows = [(21948749 , 'rs4796793'  , 'Carcinoma, Non-Small-Cell Lung' , 'ERCC1', 0.0152),
            (23059779 , 'rs12983047' , 'Carcinoma, Non-Small-Cell Lung' , 'TP53', 0.0173),
            (21995493 , 'rs652625'   , 'Lung Neoplasms'      , 'TTF1', 0.0231),
            (23059779 , 'rs12983047' , 'Carcinoma, Squamous Cell' , 'TP53', 0.0173),
            (17602083 , 'rs4796793'  , 'Neoplasm Metastasis' , 'CEACAM7', 0.0459), 
            (21995493 , 'rs652625'   , 'Carcinoma, Non-Small-Cell Lung' , 'ERCC1', 0.0380), 
            (21948749 , 'rs4796793'  , 'Lung Neoplasms'      , 'CEACAM7', 0.0459), 
            (22344756 , 'rs3213182'  , 'Carcinoma, Squamous Cell' , 'PDXP', 0.0326),
            (21995493 , 'rs652625'   , 'Lung Neoplasms'      , 'CEACAM7', 0.0359), 
            (17602083 , 'rs4796793'  , 'Carcinoma, Renal Cell' , 'XRCC1', 0.0149), 
            (21948749 , 'rs4796793'  , 'Lung Neoplasms'      , 'CEACAM5', 0.0008)]
    pubmed_id, gv_snps, gv_dis, gene, pval = zip(*table_rows)

    data, all_sd = load_data(orig_file)
    print('Generating top hits in %s' % out_name)
    out_file = csv.writer(gzip.open(out_name,'w'), delimiter='\t')
    names = ['PubMedID','SNP','Disease','SNPchr','SNPloc','Gene','GeneWindow',
             'eQTL','HiCLR','Dist','GeneID','LitScore', 'dbSNPFreq',
             'rankHiCLR','rankLit','rankAvg','Total','p-value']
    out_file.writerow(names)

    num_rows = 0
    for sd in all_sd:
        snp, disease = sd.split('|')
        idx = np.logical_and(data['SNP']==snp, data['Disease']==disease)
        if snp in gv_snps: 
            cur_data = data[idx]
            to_show = False
            for item_idx, item in enumerate(cur_data):
                for entry in table_rows:
                    if (item['PubMedID'] == entry[0] and
                        item['SNP'] == entry[1] and
                        item['Disease'] == entry[2] and
                        item['Gene'] == entry[3]):
                        to_show = True
                        break
                if to_show:
                    print item
                    _row_table(out_file, cur_data, item_idx, entry[4])
                    to_show = False
                    num_rows += 1
    print num_rows

def _row_table(out_file, cur_data, item_idx, pval):
    hic_rank = get_rank_hic(cur_data['HiCLR'])
    lit_rank = get_rank_lit(cur_data['LitScore'])
    num_ex = len(cur_data)
    avg_rank = geo_mean(hic_rank, lit_rank, num_ex)

    line = list(cur_data[item_idx])
    line.append(hic_rank[item_idx])
    line.append(lit_rank[item_idx])
    line.append(avg_rank[item_idx])
    line.append(num_ex)
    line.append(pval)
    out_file.writerow(line)
    
                
if __name__ == '__main__':
    eqtl_rank('validation_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz',
              'eqtlonly_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz')
    k = 10
    top_avg('discovery_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz',
            'top%d_diffchrom_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz' % k, k)
    genevar_validation('discovery_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz',
                       'discovery_genevar.tsv.gz')
