
import gzip
import csv
import numpy as np
from ranktools import *

def same_chrom(orig_file, out_name):
    data, all_sd = load_data(orig_file)

    print('Far SNPs in %s' % out_name)
    out_file = csv.writer(gzip.open(out_name,'w'), delimiter='\t')
    names = ['PubMedID','SNP','Disease','SNPchr','SNPloc','Gene','GeneWindow',
             'eQTL','HiCLR','Dist','GeneID','LitScore','dbSNPFreq']
    out_file.writerow(names)
    for line in data:
        if line['Dist'] >= 500000:
            out_file.writerow(line)

            #chrom, loc = line['GeneWindow'].split(':')
            #cur_loc = line['SNPloc']
            #start, finish = map(int, loc.split('-'))
            #if ((cur_loc < start) and (cur_loc > (start-1000000)) and (cur_loc > 0)
            #      or (cur_loc > finish) and (cur_loc < (finish+1000000))):
            
def diff_chrom(orig_file, out_name):
    data, all_sd = load_data(orig_file)

    print('Diff chrom genes in %s' % out_name)
    out_file = csv.writer(gzip.open(out_name,'w'), delimiter='\t')
    names = ['PubMedID','SNP','Disease','SNPchr','SNPloc','Gene','GeneWindow',
             'eQTL','HiCLR','Dist','GeneID','LitScore','dbSNPFreq']
    out_file.writerow(names)
    for line in data:
        if line['Dist'] < 0:
            out_file.writerow(line)

def analyse(snp, disease, data):
    idx_eqtl = np.flatnonzero(data['eQTL']>0)
    print('SNP: %s, Dis: %s, nnz(eQTL)=%d' % (snp,disease,len(idx_eqtl)))
    dist_rank = get_rank_dist(data['Dist'])
    #cooc_rank = get_rank_count(data['dbSNPFreq'])
    hic_rank = get_rank_hic(data['HiCLR'])
    lit_rank = get_rank_lit(data['LitScore'])
    #avg_rank = geo_mean3(dist_rank, hic_rank, lit_rank, len(data['HiCLR']))[idx_eqtl]
    avg_rank = geo_mean(hic_rank, lit_rank, len(data['HiCLR']))[idx_eqtl]
    hic_rank = hic_rank[idx_eqtl]
    lit_rank = lit_rank[idx_eqtl]
    dist_rank = dist_rank[idx_eqtl]
    return idx_eqtl, dist_rank, hic_rank, lit_rank, avg_rank

def have_cooc(orig_file, out_name):
    data, all_sd = load_data(orig_file)
    print('Have abstract mentions in %s' % out_name)
    out_file = csv.writer(gzip.open(out_name,'w'), delimiter='\t')
    names = ['PubMedID','SNP','Disease','SNPchr','SNPloc','Gene','GeneWindow',
             'eQTL','HiCLR','Dist','GeneID','LitScore','dbSNPFreq']
    out_file.writerow(names)
    for line in data:
        if line['dbSNPFreq'] > 0:
            out_file.writerow(line)

def have_eqtl(in_file, out_name):
    data, all_sd = load_data(in_file)
    print('With eQTL in %s' % out_name)
    out_file = csv.writer(open(out_name,'w'), delimiter='\t')
    names = ['PubMedID','SNP','Disease','SNPchr','SNPloc','Gene','GeneWindow',
             'eQTL','HiCLR','Dist','GeneID','LitScore','dbSNPFreq',
             'rankDist','rankHiCLR','rankLit','rankAvg','TotalEx']
    out_file.writerow(names)

    for sd in all_sd:
        snp, disease = sd.split('|')
        idx = np.logical_and(data['SNP']==snp, data['Disease']==disease)
        cur_data = data[idx]
        num_ex = len(cur_data)
        ranks = analyse(snp, disease, cur_data)
        idx_eqtl, dist_rank, hic_rank, lit_rank, avg_rank = ranks
        if len(dist_rank) == 0:
            continue
        print cur_data[idx_eqtl]
        for ix,row in enumerate(cur_data[idx_eqtl]):
            line = list(row)
            line.append(dist_rank[ix])
            line.append(hic_rank[ix])
            line.append(lit_rank[ix])
            #line.append(cooc_rank[ix])
            line.append(avg_rank[ix])
            line.append(num_ex)
            out_file.writerow(line)
            
        
def have_eqtl_norank(orig_file, out_name):
    """The entries that have eQTL column nonzero, and in the same chromosome"""
    data, all_sd = load_data(orig_file)
    out_file = csv.writer(open(out_name, 'w'), delimiter='\t')
    names = ['PubMedID','SNP','Disease','SNPchr','SNPloc','Gene','GeneWindow',
             'eQTL','HiCLR','Dist','GeneID','LitScore','dbSNPFreq']
    out_file.writerow(names)

    for sd in all_sd:
        snp, disease = sd.split('|')
        idx = np.logical_and(data['SNP']==snp, data['Disease']==disease)
        cur_data = data[idx]
        ranks = analyse(snp, disease, cur_data)
        idx_eqtl, dist_rank, hic_rank, lit_rank, avg_rank = ranks
        if len(dist_rank) == 0:
            continue
        for ix,row in enumerate(cur_data[idx_eqtl]):
            if np.isnan(row['LitScore']) or row['Dist']<0:
                continue
            line = list(row)
            out_file.writerow(line)
            continue
        

if __name__ == '__main__':
    base_dir = '/Users/cong/Dropbox/NICTA_2014_ISMB/data/'
    have_eqtl_norank(base_dir+'performance_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz',
              'alleqtl.tsv')
    
    #same_chrom('/Users/cong/Dropbox/NICTA_2014_ISMB/data/performance_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz', 'validation_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz')

    #diff_chrom('/Users/cong/Dropbox/NICTA_2014_ISMB/data/performance_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz', 'discovery_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz')
    #have_eqtl('samechrom_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz',
    #          'witheQTL_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv')

    #have_eqtl('validation_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz',
    #          'valeQTL_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv')
    #have_eqtl('/Users/cong/Dropbox/NICTA_2014_ISMB/data/performance_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz',               'alleQTL_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv')
    #have_cooc('/Users/cong/Dropbox/NICTA_2014_ISMB/data/performance_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz', 'withCooc_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv')
