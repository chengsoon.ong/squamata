"""Analysis of HiC, literature mining and eQTL ranking of disease causing SNPs"""

import csv
import gzip
import numpy as np
from optwok.io_pickle import load, save
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from ranktools import *

def analyse(snp, disease, data):
    idx_eqtl = np.flatnonzero(data['eQTL']>0)
    #print('SNP: %s, Dis: %s, nnz(eQTL)=%d' % (snp,disease,len(idx_eqtl)))
    dist = snp_in_window(data[idx_eqtl])
    dist_rank = get_rank_dist(data['Dist'])
    hic_rank = get_rank_hic(data['HiCLR'])
    lit_rank = get_rank_lit(data['LitScore'])
    avg_rank = geo_mean3(dist_rank, hic_rank, lit_rank, len(data['HiCLR']))[idx_eqtl]
    #avg_rank = geo_mean(dist_rank, lit_rank, len(data['Dist']))[idx_eqtl]
    #avg_rank = geo_mean(hic_rank, lit_rank, len(data['HiCLR']))[idx_eqtl]
    hic_rank = hic_rank[idx_eqtl]
    lit_rank = lit_rank[idx_eqtl]
    dist_rank = dist_rank[idx_eqtl]
    return dist, dist_rank, hic_rank, lit_rank, avg_rank

def snp_in_window(data):
    dist = []
    for item in data:
        in_window = np.inf
        chrom, loc = item['GeneWindow'].split(':')
        if chrom == item['SNPchr']:
            start, finish = map(int, loc.split('-'))
            cur_loc = item['SNPloc']
            if (cur_loc >= start) and (cur_loc <= finish):
                in_window = int(item['Dist'])
            elif ((cur_loc < start) and (cur_loc > (start-1000000)) and (cur_loc > 0)
                  or (cur_loc > finish) and (cur_loc < (finish+1000000))):
                in_window = -1
        dist.append(in_window)
        #print in_window, item['SNPchr'], item['SNPloc'], item['GeneWindow']
    return np.array(dist)

def get_eqtl(data_file):
    data, all_sd = load_data(data_file)
    print('Statistics on %d SNP disease pairs' % len(all_sd))
    all_dists = []
    all_ranks_dist = []
    all_ranks_hic = []
    all_ranks_lit = []
    all_ranks_avg = []
    num_ex = []
    for sd in all_sd:
        snp, disease = sd.split('|')
        # This is a SNP that is very close to rs2029166 and is in the same gene.
        if snp ==  'rs7296239':
            print 'rs7296239'
            continue
        idx = np.logical_and(data['SNP']==snp, data['Disease']==disease)
        num_ex.append(len(np.flatnonzero(idx)))
        dist, dist_rank, hic_rank, lit_rank, avg_rank = analyse(snp, disease, data[idx])
        all_dists.append(dist)
        all_ranks_dist.append(dist_rank)
        all_ranks_hic.append(hic_rank)
        all_ranks_lit.append(lit_rank)
        all_ranks_avg.append(avg_rank)

    save('ranks_eqtl.pkl', {'dataset': all_sd, 'num_ex': num_ex, 'dists': all_dists,
                            'ranks_avg': all_ranks_avg,
                       'ranks_dist': all_ranks_dist, 'ranks_hic': all_ranks_hic, 'ranks_lit': all_ranks_lit})


    
def gen_markers():
    markers = []
    for colour in ['b','g','r','c','m','b','g','r','c','m']:
        for mark in ['o','v','^','<','>','s','p','*','h','D']:
            markers.append(colour+mark)
    return np.array(markers)
        
def show_results(results, threshold, dist, pdf_name, score_name):
    markers = gen_markers()
    fig = plt.figure(figsize=(9,12))
    ax = fig.add_subplot(111)
    for ix, ranks in enumerate(results[score_name]):
        for ixr, item in enumerate(ranks):
            if np.isinf(dist) and np.isinf(results['dists'][ix][ixr]):
                ax.plot(item, ix, markers[ix])
            elif dist == 1 and results['dists'][ix][ixr] >= 0:
                ax.plot(item, ix, markers[ix])
            elif dist == -1 and results['dists'][ix][ixr] == dist:
                ax.plot(item, ix, markers[ix])
            elif dist == -2:
                ax.plot(item, ix, markers[ix])

            if item > threshold:
                print('%d:Not shown: %s : %d' % (dist, results['dataset'][ix], item))

    ax.axis([0, threshold, -1, len(results[score_name])])
    ax.grid()
    ax.set_xlabel('Rank of eQTL')
    ax.set_yticks(range(len(results[score_name])))
    ax.set_yticklabels(results['dataset'])
    ax.set_title(pdf_name)
    plt.tight_layout()
    plt.savefig(pdf_name+'.pdf')
    plt.close(fig)

def print_trans(results, txt_name, score_name):
    out_file = open(txt_name, 'w')
    for ix, ranks in enumerate(results[score_name]):
        for ixr, item in enumerate(ranks):
            if np.isinf(results['dists'][ix][ixr]):
                out_file.write('%s : %d\n' % (results['dataset'][ix], item))
    out_file.close()
    
def combine(data1, data2, num_ex):
    comb = []
    for items in zip(data1, data2, num_ex):
        comb.append(geo_mean(items[0], items[1], items[2]))
    return comb

def plot_per_sd():
    results = load('ranks_eqtl.pkl')
    show_results(results, 100, -2, 'ranks_dist', 'ranks_dist')
    show_results(results, 100, 1, 'ranks_dist_cis', 'ranks_dist')
    show_results(results, 100, -1, 'ranks_dist_neighbour', 'ranks_dist')
    print_trans(results, 'ranks_dist_trans.txt', 'ranks_dist')

    show_results(results, 1000, -2, 'ranks_HiCLR', 'ranks_hic')
    show_results(results, 1000, 1, 'ranks_HiCLR_cis', 'ranks_hic')
    show_results(results, 1000, -1, 'ranks_HiCLR_neighbour', 'ranks_hic')
    print_trans(results, 'ranks_HiCLR_trans.txt', 'ranks_hic')

    show_results(results, 1000, -2, 'ranks_LitScore', 'ranks_lit')
    show_results(results, 1000, 1, 'ranks_LitScore_cis', 'ranks_lit')
    show_results(results, 1000, -1, 'ranks_LitScore_neighbour', 'ranks_lit')
    print_trans(results, 'ranks_LitScore_trans.txt', 'ranks_lit')

    show_results(results, 100, -2, 'ranks_geomean', 'ranks_avg')
    show_results(results, 100, 1, 'ranks_geomean_cis', 'ranks_avg')
    show_results(results, 100, -1, 'ranks_geomean_neighbour', 'ranks_avg')
    print_trans(results, 'ranks_geomean_trans.txt', 'ranks_avg')

def plot_in_same(score_name='ranks_hic'):
    results = load('ranks_eqtl.pkl')
    markers = gen_markers()
    fig = plt.figure(figsize=(6,6))
    ax = fig.add_subplot(111)
    for ix, ranks in enumerate(results[score_name]):
        for ixr, item in enumerate(ranks):
            dist = results['dists'][ix][ixr]
            if not np.isinf(dist):
                ax.plot(item, results['ranks_dist'][ix][ixr], markers[ix])
    ax.grid()
    ax.set_xlabel('Rank of HiC')
    ax.set_ylabel('Rank of genomic distance')
    ax.axis([0,100,0,100])
    plt.savefig('scatter_hic_dist.pdf')
    
def collect_ranks(data_file):
    data, all_sd = load_data(data_file)
    print('Statistics on %d SNP disease pairs' % len(all_sd))
    all_ranks_hic = []
    all_ranks_lit = []
    all_ranks_avg = []
    all_ranks_dist = []
    for sd in all_sd:
        snp, disease = sd.split('|')
        idx = np.logical_and(data['SNP']==snp, data['Disease']==disease)
        cur_data = data[idx]
        num_ex = len(cur_data)
        dist_rank = get_rank_dist(cur_data['Dist'])
        hic_rank = get_rank_hic(cur_data['HiCLR'])
        lit_rank = get_rank_lit(cur_data['LitScore'])
        all_ranks_dist.append(dist_rank)
        all_ranks_hic.append(hic_rank)
        all_ranks_lit.append(lit_rank)
        all_ranks_avg.append(geo_mean(hic_rank, lit_rank, num_ex))
        
    save('ranks_all.pkl', {'dataset': all_sd, 'ranks_hic': all_ranks_hic, 
                           'ranks_lit': all_ranks_lit, 'ranks_avg': all_ranks_avg,
                           'ranks_dist': all_ranks_dist})

def plot_hic_lit():
    results = load('ranks_all.pkl')
    markers = gen_markers()
    for ix in range(len(results['dataset'])):
        print('Plotting %s' % results['dataset'][ix])
        fig = plt.figure(figsize=(6,6))
        ax = fig.add_subplot(111)
        ax.plot(results['ranks_hic'][ix], results['ranks_lit'][ix], markers[ix])
        ax.set_title(results['dataset'][ix])
        ax.set_xlabel('Rank based on HiCLR')
        ax.set_ylabel('Rank based on Literature')
        plt.savefig('hic_lit_%d.pdf' % ix)
        plt.close(fig)

def names(raw_string):
    name = str(raw_string[6:])
    translate = {'dist': 'gen\_dist',
                 'lit': 'lit',
                 'hic': 'spatial',
                 }
    return translate[name]
        
def scatter_perf():
    results = load('ranks_eqtl.pkl')

    titles = ['(a)','(b)','(c)','(d)','(e)','(f)']
    # Compare ranks pairwise
    fig = plt.figure(figsize=(20,13))
    pairs = [('ranks_dist','ranks_hic'),('ranks_dist','ranks_lit'),('ranks_hic','ranks_lit')]
    for ix,pair in enumerate(pairs):
        ax = fig.add_subplot(2,3,ix+1, aspect='equal')
        for ix1,r1 in enumerate(results[pair[0]]):
            for ix2,r2 in enumerate(r1):
                ax.plot(r2, results[pair[1]][ix1][ix2], 'bo', alpha=0.5)
        ax.set_xlabel('Rank based on $rank_{%s}$' % names(pair[0]))
        ax.set_ylabel('Rank based on $rank_{%s}$' % names(pair[1]))
        ax.plot([0,1],[0,1],'k:')
        ax.axis([0,1,0,1])
        ax.set_title(titles[ix], loc='left')

    for ix,pair in enumerate(pairs):
        ax = fig.add_subplot(2,3,ix+4, aspect='equal')
        for ix1,r1 in enumerate(results[pair[0]]):
            for ix2,r2 in enumerate(r1):
                ax.plot(r2, results[pair[1]][ix1][ix2], 'bo', alpha=0.5)
        ax.set_xlabel('Rank based on $rank_{%s}$' % names(pair[0]))
        ax.set_ylabel('Rank based on $rank_{%s}$' % names(pair[1]))
        ax.plot([0,1],[0,1],'k:')
        ax.axis([0,0.1,0,0.1])
        ax.set_title(titles[ix+3], loc='left')

    fig_file = 'scatter_pairwise'
    plt.savefig(fig_file+'.eps', bbox_inches='tight')
    plt.savefig(fig_file+'.pdf', bbox_inches='tight')
    plt.close()
        
    # Compare each rank to the combined average rank
    pg = Polygon(np.array([[0,0], [0,1], [1,1]]), closed=True, facecolor='green', alpha=0.2)
    pr = Polygon(np.array([[0,0], [1,0], [1,1]]), closed=True, facecolor='red', alpha=0.2)
    sources = ['ranks_dist','ranks_hic','ranks_lit']
    fig = plt.figure(figsize=(20,13))
    for idx,source in enumerate(sources):
        ax = fig.add_subplot(2,3,idx+1, aspect='equal')
        for ix,rd in enumerate(results[source]):
            for ixr,rank in enumerate(rd):
                ax.plot(results['ranks_avg'][ix][ixr], rank, 'bo', alpha=0.5)
        ax.set_ylabel('Rank based on $rank_{%s}$' % names(source))
        ax.set_xlabel('Rank based on $rank_{hybrid}$')
        ax.plot([0,1],[0,1],'k:')
        ax.axis([0,1,0,1])
        pg = Polygon(np.array([[0,0], [0,1], [1,1]]), closed=True, facecolor='green', alpha=0.2)
        pr = Polygon(np.array([[0,0], [1,0], [1,1]]), closed=True, facecolor='red', alpha=0.2)
        ax.add_patch(pg)
        ax.add_patch(pr)
        ax.set_title(titles[idx], loc='left')
        
    sources = ['ranks_dist','ranks_hic','ranks_lit']
    for idx,source in enumerate(sources):
        ax = fig.add_subplot(2,3,idx+4, aspect='equal')
        for ix,rd in enumerate(results[source]):
            for ixr,rank in enumerate(rd):
                ax.plot(results['ranks_avg'][ix][ixr], rank, 'bo', alpha=0.5)
        ax.set_ylabel('Rank based on $rank_{%s}$' % names(source))
        ax.set_xlabel('Rank based on $rank_{hybrid}$')
        ax.plot([0,1],[0,1],'k:')
        ax.axis([0,0.1,0,0.1])
        pg = Polygon(np.array([[0,0], [0,1], [1,1]]), closed=True, facecolor='green', alpha=0.2)
        pr = Polygon(np.array([[0,0], [1,0], [1,1]]), closed=True, facecolor='red', alpha=0.2)
        ax.add_patch(pg)
        ax.add_patch(pr)
        ax.set_title(titles[idx+3], loc='left')
    fig_file = 'scatter_avg'
    plt.savefig(fig_file+'.eps', bbox_inches='tight')
    plt.savefig(fig_file+'.pdf', bbox_inches='tight')
    plt.close()
        
def compare_perf():
    results = load('ranks_eqtl.pkl')
    all_ranks_avg = []
    all_ranks_dist = []
    all_ranks_hic = []
    all_ranks_lit = []
    for ix in range(len(results['ranks_avg'])):
        for iy in range(len(results['ranks_avg'][ix])):
            all_ranks_avg.append(results['ranks_avg'][ix][iy])
            all_ranks_dist.append(results['ranks_dist'][ix][iy])
            all_ranks_hic.append(results['ranks_hic'][ix][iy])
            all_ranks_lit.append(results['ranks_lit'][ix][iy])
    all_ranks_avg = np.array(all_ranks_avg)         
    all_ranks_dist = np.array(all_ranks_dist)
    all_ranks_hic = np.array(all_ranks_hic)
    all_ranks_lit = np.array(all_ranks_lit)
    #first_dist = all_ranks_dist == 1
    #all_ranks_avg = all_ranks_avg[~first_dist]
    #all_ranks_dist = all_ranks_dist[~first_dist]
    print('Is average better than genomic distance?')
    print(all_ranks_avg)
    print(all_ranks_dist)
    better = all_ranks_avg <= all_ranks_dist
    print(better)         
    print(len(np.flatnonzero(better)), len(better))

    print('Is average better than hic?')
    print(all_ranks_avg)
    print(all_ranks_hic)
    better = all_ranks_avg <= all_ranks_hic
    print(better)         
    print(len(np.flatnonzero(better)), len(better))
    
    print('Is average better than literature?')
    print(all_ranks_avg)
    print(all_ranks_lit)
    better = all_ranks_avg <= all_ranks_lit
    print(better)         
    print(len(np.flatnonzero(better)), len(better))
    
if __name__ == '__main__':
    # Plot rank of eQTL hit for HiC, LitScore and geometric mean of both
    #get_eqtl('/Users/cong/Dropbox/NICTA_2014_ISMB/data/performance_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz')
    #get_eqtl('validation_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz')
    #plot_per_sd()
    #plot_in_same()
    scatter_perf()
    #compare_perf()

    # Scatter plot of HiC vs LitScore per SNP disease pair
    #collect_ranks('/Users/cong/Dropbox/NICTA_2014_ISMB/data/performance_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz')
    #plot_hic_lit()

