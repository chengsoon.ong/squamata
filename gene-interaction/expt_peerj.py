"""Script to generate the results for the gene interaction paper with
   Geoff, Antonio and Karin.

   This script filters the list of associations into two sets:
   1. validation
   2. discovery

   The validation set consists of associations that are "far away" (>500kb)
    on the same chromosome, that have eQTL data.
   The discovery set consists of associations on different chromosomes
   """

from filter_same_chrom import diff_chrom, same_chrom
from seq_lit_perf import get_eqtl, scatter_perf, compare_perf
from top_average import top_avg, eqtl_rank, genevar_validation
from tsv2json import convert_tsv2json

if __name__ == "__main__":
    base_dir = '/Users/cong/Dropbox/NICTA_2014_ISMB/data/'

    # Create the validation set by considering interactions in the same chromosome
    # but more than 500Kb away.
    same_chrom(base_dir+'performance_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz',
               'validation_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz')    
    eqtl_rank('validation_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz',
              'eqtlonly_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz')
    get_eqtl('validation_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz')
    scatter_perf()
    compare_perf()

    # The discovery set consists of interactions between chromosomes
    diff_chrom(base_dir+'/performance_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz',
               'discovery_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz')
    genevar_validation('discovery_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz',
                       'discovery_genevar.tsv.gz')
    k = 10
    top_avg('discovery_dat_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz',
            'top%d_diffchrom_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz' % k, k)
    convert_tsv2json('top10_diffchrom_eQTL_HiC_dist.lit.NCBI.upmid.tsv.gz',
                     'top10_diffchrom.json')
