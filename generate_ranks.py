"""Code to generate ranked lists.
Ranks start from 1.
"""
import numpy
from numpy import array, empty, flatnonzero, zeros, concatenate
from numpy import log, exp, argsort, power, sqrt
from numpy.random import rand, permutation, seed
import csv
from numpy.random import exponential, gamma

def get_filename(kap, ext):
    """Generate a filename"""
    return '%s_%2.1f_%s_%1.1f_%d_%d.%s' %\
        (method_top, theta, method_bottom, delta, kap, num_items, ext)

def perfect(start_rank, stop_rank):
    """Return a perfectly ordered ranked list"""
    return range(start_rank, stop_rank)

def random(start_rank, stop_rank):
    """Return a random permutation of ranks between start and stop"""
    seed()
    return (permutation(stop_rank-start_rank)+start_rank+1).tolist()

def plot_copula(u,v):
    """Visualize the copula for debugging"""
    from matplotlib.pyplot import figure, show

    plt = figure(figsize=(8,8))
    #ax = plt.add_subplot(111, aspect='equal')
    ax = plt.add_subplot(111)
    ax.plot(u, v, 'b.', alpha=0.2)
    show()

def map_to_id(ground, u, v):
    """Given samples from a copula (u,v), convert them to ranks (uid, vid).
    Map the identities uid to the ground truth list.
    Permute vid the same way.
    """
    uid = argsort(argsort(u))+1
    vid = argsort(argsort(v))+1
    output = []
    for elem in ground:
        idx = flatnonzero(uid==elem)[0]
        output.append(vid[idx])
    return output

def cond_copula_frank(u, t, theta):
    """Generate a Frank copula over the ranks 1 to kappa,
    and a random permutation from kappa till total_ranks.
    Uses the conditional copula method.
    Based on the appendix (Table A.1) from
    Copula Modeling: An Introduction for Practitioners.
    """
    numer = t*(1-exp(-theta))
    eu = exp(-theta*u)
    denom = t*(eu - 1.) - eu
    v = -(1./theta)*log(1.+numer/denom)
    return v

def generate_frank(kappa, total_ranks, theta=25.0):
    pass

def cond_copula_fgm(ground, theta):
    """Conditional copula for Farlie Gumbel Morgenstein copula
    returns a ranked list which is dependent on the ground list"""
    num_items = len(ground)
    
    #u = array(ground,dtype=float)/float(num_items+1)
    u = rand(num_items)
    t = rand(num_items)
    A = theta*(2.*u-1.)
    B = (1.-A)*(1.-A) + 4*A*t
    v = 2.*t/(sqrt(B) - A)
    #v[v > 1.] = 1.
    #v[v < 0.] = 0.
    return (argsort(argsort(v))+1).tolist()

def generate_fgm(kappa, total_ranks, theta=1.0):
    """Generate a Farlie Gumbel Morgenstein copula over the ranks 1 to kappa,
    and a random permutation from kappa till total_ranks.
    """
    ground = eval('%s(%d, %d)' % (method_top, 1, kappa))
    ground.extend(eval('%s(%d, %d)' % (method_bottom, kappa, total_ranks+1)))
    data = cond_copula_fgm(ground, theta)
    return data
    
def generate_clayton(kappa, total_ranks, theta=0.5):
    """Generate a Clayton copula over the ranks 1 to kappa,
    and a random permutation from kappa till total_ranks.
    Based on Exercise 4.17 (page 135) in Nelsen, 2006.
    """
    x = exponential(1., size=total_ranks+1)
    y = exponential(1., size=total_ranks+1)
    z = gamma(theta, 1., size=total_ranks+1)
    u = power(1.+(x/z), -theta)
    v = power(1.+(y/z), -theta)
    ground = eval('%s(%d, %d)' % (method_top, 1, kappa))
    ground.extend(eval('%s(%d, %d)' % (method_bottom, kappa, total_ranks+1)))
    return map_to_id(ground, u, v)

def generate_copula_mix(method_top, method_bottom, kappa, num_items, theta):
    """Generate a mixture of copulas.
    The upper right corner is generated using method_bottom,
    and the rest (left and bottom slices) are generated using method_top.
    kappa defines the transition between method_top and method_bottom,
    and theta is the parameter (currently passed to method_top).
    Returns num_items of pairs (u,v) which is drawn from the copula mixture.
    """
    u = rand(num_items)
    t = rand(num_items)
    v = zeros(num_items)
    frac = float(kappa)/float(num_items)
    top = numpy.logical_or(u<frac,t<frac)
    v[top] = eval('cond_copula_%s(u[top], t[top], theta)' % method_top)
    bottom = numpy.logical_not(top)
    v[bottom] = t[bottom]
    return (u,v)
    
def generate_copula(hidden, method_top, method_bottom, kappa, num_items, theta, overlap):
    """Generate a mixture of copulas using an offset of two copulas.
    method_top takes parameter theta generating a copula.
    method_top generates kappa pairs (u,v)
    method_bottom generates num_items-kappa pairs (u,v), currently assumed to be random
    overlap ranges between 0 (no overlap) and 1 (full overlap) between
      the two copulas method_top and method_bottom.
    Returns num_items of pairs (u,v) which is drawn from the copula mixture.
    """
    ut = hidden[:kappa]
    ub = hidden[kappa:] + (1.-overlap)

    tt = rand(kappa)
    vt = eval('cond_copula_%s(ut, tt, theta)' % method_top)
    vb = rand(num_items-kappa) + (1.-overlap)
    u = concatenate([ut, ub])/(2.-overlap)
    v = concatenate([vt, vb])/(2.-overlap)
    return (u,v)
    

def generate_data(num_draws=50):
    """Main loop to generate all data"""
    for kap in kappa:
        ground = array(range(num_items), dtype=int)+1
        hidden = rand(num_items)
        file_name = get_filename(kap, 'txt')
        print('Generating %s' % file_name)
        fp = open(file_name, 'w')
        out_file = csv.writer(fp, delimiter=',')
        for draw in range(num_draws):
            (u,v) = generate_copula(hidden, method_top, method_bottom, kap, num_items, theta, delta)
            data = map_to_id(ground, u, v)
            out_file.writerow(data)
        fp.close()

if __name__ == '__main__':
    method_top = 'frank'
    method_bottom = 'random'
    kappa = [100, 200, 500]
    num_items = 2000
    theta = 15.0
    delta = 0.2
    
    generate_data()
    
