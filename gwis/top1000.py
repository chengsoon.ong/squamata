"""Extract the first 1004 lines from each file in the archive.
First 4 lines should be header lines.
"""

import tarfile
import cStringIO
import time

def head(in_stream, num_lines=1004):
    out_stream = cStringIO.StringIO()
    for ix,line in enumerate(in_stream):
        if ix >= num_lines:
            break
        out_stream.write(line)
    out_stream.seek(0)
    return out_stream

if __name__ == '__main__':
    in_filename = '/Users/cong/temp/ranks.tar.bz2'
    out_filename = '/Users/cong/Data/WTCCC/top1000/ranks1000.tar.bz2'
    in_file = tarfile.open(in_filename, 'r|bz2')
    out_file = tarfile.open(out_filename, 'w|bz2')
    
    while True:
        cur_file = in_file.next()
        if cur_file is None:
            break
        handle = in_file.extractfile(cur_file)
        print(handle.name)
        if handle.name.startswith('raWTC'):
            print 'Taking top 10000'
            file_head = head(handle, 10004)
        else:
            file_head = head(handle)

        tarinfo = tarfile.TarInfo(name=handle.name)
        tarinfo.size = len(file_head.getvalue())
        tarinfo.mtime = time.time()
        
        out_file.addfile(tarinfo, file_head)
        file_head.close()

    in_file.close()
    out_file.close()
    
        
