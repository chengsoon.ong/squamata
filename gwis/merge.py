"""Merge the files containing lists of pairs into one csv file per dataset.

Read with
df = pd.read_table(bz2.BZ2File('top1000_pairs.csv.bz2','r'), sep=',', index_col=(0,1,2,3,4,5))
"""

import tarfile
import pandas as pd
import numpy as np
import bz2

def merge(data_dir):
    prune = [False, True]
    split = range(10)
    cv = [1,2]
    stat = ['Chi2', 'GSS']

    # All this mess is so that we have 10000 scores for ra and 1000 for the other datasets.
    dataset = ['ra']    
    score = range(10000)
    multi_idx = pd.MultiIndex.from_product([dataset,prune,split,cv,stat,score],
                                            names=['dataset','prune','split','cv','stat','score'])
    df = pd.DataFrame(index=multi_idx, columns=['prb1','prb2','stat'])
    dataset = ['bd', 'cad', 'cd', 'ht', 't1d', 't2d']
    score = range(1000)
    multi_idx = pd.MultiIndex.from_product([dataset,prune,split,cv,stat,score],
                                            names=['dataset','prune','split','cv','stat','score'])
    df = pd.concat([df, pd.DataFrame(index=multi_idx, columns=['prb1','prb2','stat'])])
    # pandas! indices need to be sorted for slicing to work
    df = df.sortlevel(level='dataset')
    print(df.index.levels)
    dataset = ['bd', 'cad', 'cd', 'ht', 'ra', 't1d', 't2d']
    tf = tarfile.open('%s/ranks1000.tar.bz2' % data_dir, 'r:bz2')
    all_files = []
    for d in dataset:
        for p in prune:
            for n in split:
                for c in cv:
                    for s in stat:
                        if p:
                            name = '%sWTCp_split%d_cv%dof2_sel_%s.txt' % (d,n,c,s)
                        else:
                            name = '%sWTC_split%d_cv%dof2_sel_%s.txt' % (d,n,c,s)
                        print(name)
                        handle = tf.extractfile(name)
                        data = pd.read_table(handle, header=3, sep='\t', usecols=[0,1,2])
                        df.loc[(d,p,n,c,s),:] = np.array(data)
    return df

if __name__ == '__main__':
    data_dir = '/Users/cong/Data/WTCCC/top1000'
    df = merge(data_dir)
    handle = bz2.BZ2File('%s/top1000_pairs.csv.bz2' % data_dir, 'w')
    df.to_csv(handle)
    handle.close()
