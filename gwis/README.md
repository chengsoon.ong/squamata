Slice the top 1000 pairs from the 40GB archive (ranks.tar.bz2)::

	python top1000.py

Merge files from the tar archive (ranks1000.tar.bz2) into one csv file (top1000_pairs.csv.bz2)::

	python merge.py

"Rotate" the data such that for each dataset, we have a unique list of pairs for all splits::

	python pairs2id.py

