"""For each dataset, prune, statistic tuple, create one table
where we have an identifier for each pair"""

import numpy as np
import pandas as pd
import bz2

def header(split, cv):
    """Create the column index"""
    a = ['prb1','prb2']
    for s in split:
        for c in cv:
            a.append('s%dc%d' % (s,c))
    return a

def all_pairs(data):
    unique_pairs = data[['prb1','prb2']].copy()
    unique_pairs = unique_pairs.drop_duplicates()
    unique_pairs = unique_pairs.sort(['prb1','prb2'])
    return np.array(unique_pairs, dtype=int)
    
def pairs2id(all_data, d, p, s):
    """Reformat the data"""
    data = all_data.query('dataset == "%s" and prune == %s and stat == "%s"' % (d,p,s))
    splits = range(10)
    cvs = [1,2]

    col_idx = header(splits, cvs)
    unique_pairs = all_pairs(data)
    score_df = pd.DataFrame(index=range(len(unique_pairs)),columns=col_idx)
    score_df['prb1'] = unique_pairs[:,0]
    score_df['prb2'] = unique_pairs[:,1]
    rank = pd.DataFrame(index=range(len(unique_pairs)),columns=col_idx)
    rank['prb1'] = unique_pairs[:,0]
    rank['prb2'] = unique_pairs[:,1]
    for n in splits:
        for c in cvs:
            print('%sWTC %s split%d cv%dof2 %s' % (d, str(p), n, c, s))
            if d == 'ra':
                num_pairs = 10000
            else:
                num_pairs = 1000
            for k in range(num_pairs):
                row = data.query('split == %d and cv == %d and score == %d' % (n,c,k))
                prb1 = np.array(score_df['prb1'],dtype=int)
                prb2 = np.array(score_df['prb2'],dtype=int)
                row_idx = np.flatnonzero(np.logical_and(prb1 == row['prb1'].values[0],
                                            prb2 == row['prb2'].values[0]))[0]
                score_df.loc[row_idx,'s%dc%d' % (n,c)] = row['stat.1'].values[0]
                rank.loc[row_idx,'s%dc%d' % (n,c)] = k+1
    return score_df, rank

def get_scorefile_name(d, p, s):
    if p:
        return 'top1000_%sp_%s.csv.bz2' % (d,s)
    else:
        return 'top1000_%s_%s.csv.bz2' % (d,s)

def get_rankfile_name(d, p, s):
    if p:
        return 'top1000_%sp_%s_rank.csv.bz2' % (d,s)
    else:
        return 'top1000_%s_%s_rank.csv.bz2' % (d,s)

def save(stat, rank, data_dir, d, p, s):
    handle = bz2.BZ2File(data_dir + '/' + get_scorefile_name(d, p, s), 'w')
    stat.to_csv(handle)
    handle.close()
    handle = bz2.BZ2File(data_dir + '/' + get_rankfile_name(d, p, s), 'w')
    rank.to_csv(handle)
    handle.close()
    
def process(data_dir, dataset, prune, stat):
    df = pd.read_table(bz2.BZ2File('%s/top1000_pairs.csv.bz2' % data_dir,'r'),
                        sep=',', index_col=(0,1,2,3,4,5))

    for d in dataset:
        for p in prune:
            for s in stat:
                score_df, rank = pairs2id(df, d, p, s)
                save(score_df, rank, data_dir, d, p, s)

if __name__ == '__main__':
    dataset = ['bd', 'cad', 'cd', 'ht', 'ra', 't1d', 't2d']
    prune = [False, True]
    stat = ['Chi2', 'GSS']
    data_dir = '/Users/cong/Data/WTCCC/top1000'
    
    process(data_dir, dataset, prune, stat)
    
